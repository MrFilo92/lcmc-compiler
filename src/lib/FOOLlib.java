package lib;

import interfaces.Node;
import types.*;

import java.util.ArrayList;
import java.util.HashMap;

public class FOOLlib {

	private static int labCount = 0;

	private static int funLabCount = 0;
	
	private static int methodLabCount = 0;

	private static String funCode = "";
	
	public static final int MEMSIZE = 10000;
	
	private static ArrayList<ArrayList<String>> dispatchTables = new ArrayList<ArrayList<String>>(); 

	// valuta se il tipo "a" � sottotipo al tipo "b"
	public static boolean isSubtype(Node a, Node b) {
		
		boolean result;
		
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			/* isSubtype() in FOOLlib ora deve gestire (oltre a "bool" sottotipo di "int") tipi funzionali ArrowTypeNode
			 * � entrambi devono essere ArrowTypeNode con stesso numero di parametri e deve valere:*/

			// relazione di co-varianza sul tipo di ritorno
			Node returnTypeA = ((ArrowTypeNode) a).getRet();
			Node returnTypeB = ((ArrowTypeNode) b).getRet();
			boolean resultType = FOOLlib.isSubtype(returnTypeA, returnTypeB);

			//relazione di contro-varianza sul tipo dei parametri
			ArrayList<Node> aParList = ((ArrowTypeNode) a).getParList();
			ArrayList<Node> bParList = ((ArrowTypeNode) b).getParList();

			//controllo che parametro b sia sottotipo di parametro a
			boolean resultParameter = true;
			
			for (int i = 0; i < aParList.size(); i++) {
				resultParameter &= FOOLlib.isSubtype(bParList.get(i), aParList.get(i));
			}
			result = resultType && resultParameter;
			
		}else if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			
			String aClassType = ((RefTypeNode) a).getIdClass();
			String bClassType = ((RefTypeNode) b).getIdClass();
			
			if(aClassType.equals(bClassType)) {
				//a e b sono la stessa classe
				result = true; 
			}else {
				//classe a � sottotipo di classe b
				result = false;
			}
			
		}else if (a instanceof EmptyTypeNode && b instanceof RefTypeNode) {
			result = true; //sottotipo di qualsiasi classe
		} else {
			//controllo sottotipo (a bool sottotipo di b int)
			return result = a.getClass().equals(b.getClass()) || ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode));
		}
		
		return result;
	}

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	public static String freshFunLabel() {
		return "function" + (funLabCount++);
	}

	public static String freshMethodLabel() {
		return "method" + (methodLabCount++);
	}
	
	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima della funzione
	}

	public static String getCode() {
		return funCode;
	}

	public static ArrayList<ArrayList<String>> getDispatchTables() {
		return dispatchTables;
	}

	public static void setDispatchTables(ArrayList<String> dispatchTable) {
	    FOOLlib.dispatchTables.add(dispatchTable);
	}
	
	public static ArrayList<String> getDispatchTableByOffset(int offset) {
		return dispatchTables.get(-offset-2);
	}


}

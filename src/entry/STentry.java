package entry;

import interfaces.Node;

public class STentry {
   
  private int nestingLevel; 
  private Node type;
  private int offset;
  private boolean isMethod; //aggiunta OO per distinguere ID di funzioni da ID di metodi,che richiedono l'utilizzo di 
                            //dispatch tables quando vengono invocati
  
  //caso di funzioni
  public STentry (int nl, int os) {
	  this.nestingLevel=nl;
	  this.offset=os;
  } 
  
  //caso di parametri e variabili
  public STentry (int nl, Node t, int os) {
	  this.nestingLevel=nl;
	  this.type=t;
	  this.offset=os;
  } 
  
  public void addType(Node t) {
	  type=t;
  }

  public Node getType() {
	  return type;
  }
  
  public int getOffset() {
	  return offset;
  }
  
  public int getNestinglevel() {
	  return nestingLevel;
  }  
  
  public void setNestingLevel(int n) {
    this.nestingLevel = n;
  }
  
  public void setMethod() {
	  this.isMethod = true;
  }
    
  public boolean isMethod() {
	  return isMethod;

  }
  
  public String toPrint(String s) {
	   return s+"STentry: nestlev " + Integer.toString(nestingLevel) +"\n"+
			  s+"STentry: type\n " + this.type.toPrint(s+"  ") +
			  s+"STentry: offset " + Integer.toString(offset) +"\n" +
			  s+"STentry: isMethod " + this.isMethod + "\n" ;  
  }
  
}  
package ast;

import java.util.ArrayList;
import entry.STentry;
import interfaces.Node;
import lib.FOOLlib;
import types.ClassTypeNode;
import types.RefTypeNode;

public class NewNode implements Node {

	private String id;
	private STentry entry; //STentry della classe ID in campo "entry"
						   //ID in ClassTable e STentry presa direttamente da livello 0 della Symbol Table
	private ArrayList<Node> parlist;

	public NewNode(String id, STentry entry, ArrayList<Node> parlist) {
		this.id = id;
		this.entry = entry; 
		this.parlist = parlist; 
	}

	@Override
	public String toPrint(String indent) {
		String parlstr="";
		for (Node par : parlist){
			parlstr += par.toPrint(indent+"  ");
		}

		return indent+"NewNode: "+this.id+"\n"+this.entry.toPrint(indent+"  ") + parlstr;
	}

	
	@Override
	public Node typeCheck() {
		
		ClassTypeNode classType = null;
		if(this.entry.getType() instanceof ClassTypeNode) {
			classType = (ClassTypeNode) entry.getType();
		}else {
			System.out.println("Error: invocation of a non-class"+id);
		}
		
		ArrayList<Node> fields = classType.getAllFields();
		
		//controlla che il num di parametri della entry sia uguale a quello dei par passati
	    if (!(fields.size() == this.parlist.size())){
	    	System.out.println("Wrong number of parameters in the creation of "+this.id);
	    	System.exit(0);
	    } 
	    
	     //	recupera i tipi dei parametri tramite allFields del ClassTypeNode in campo entry
	    for (int i=0; i<parlist.size(); i++) {
	    	FieldNode fieldPar = ((FieldNode)fields.get(i));    		    	
	    	if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(),fieldPar.getSymType()))) {
	    		System.out.println("New Node - Error: Wrong type for "+ i+ "-th parameter in the creation of "+id);
	    		System.exit(0);
	    	} 	    	
	    }
 
	    //se � andato tutto bene ritorna un RefTypeNode con id passato al costruttore di new node
	    RefTypeNode refType = new RefTypeNode(id);
		return refType;
	}

	@Override
	public String codeGeneration() {

		String returnCode = "";
		int dispatchPointer = FOOLlib.MEMSIZE + this.entry.getOffset(); //indirizzo dispatch pointer
		
		for(int i=0;i<this.parlist.size();i++){
			returnCode += this.parlist.get(i).codeGeneration();//si richiama su tutti i parametri in ordine di apparizione 
		}
		
		//prende i valori dei parametri, uno alla volta, dallo stack e mette nello heap, incrementando $hp dopo ogni singola copia
		for(int i=0;i<this.parlist.size();i++){
			returnCode += 	"lhp\n"+ //carico hp sullo stack
							"sw\n"+ //salvo ogni parametro che avevo caricato
							"lhp\n"+ //aggiorno hp ricaricandolo sullo stack
							"push 1\n"+ 
							"add\n"+  //incremento di 1
							"shp\n"; //salvo hp
		}
		
						
		returnCode += 	"push "+dispatchPointer +"\n" + //carico sullo stack l'indirizzo del dispatch pointer
						"lw\n"+ //carico sullo stack il dispatch pointer
						"lhp\n" + //carico sullo stack il valore di $hp (indirizzo object pointer da ritornare)
						"sw\n"+ //metto il dispatch pointer in hp
						"lhp\n"+ //lascio sullo stack il valore di hp
						"lhp\n"+ //aggiorno hp(ricaricandolo sullo stack)
						"push 1\n" +
						"add\n"+ //incremento $hp 
						"shp\n"; //salvo hp
		
		return returnCode;
	}

}

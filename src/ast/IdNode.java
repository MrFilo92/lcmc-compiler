package ast;

import entry.STentry;
import interfaces.Node;
import types.ArrowTypeNode;
import types.ClassTypeNode;

public class IdNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;

	public IdNode(String i, STentry st, int nl) {
		this.id = i; 
		this.entry = st;
		this.nestingLevel = nl;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestinglevel " + this.nestingLevel + "\n" + this.entry.toPrint(s + "  ");
	}

	public Node typeCheck() {
		/*
		 * if (entry.getType() instanceof ArrowTypeNode) {
		 * System.out.println("Wrong usage of function identifier"); System.exit(0); }
		 */
		// il controllo ora non serve pi� perch� accettiamo anche tipi funzionali

		// ID non deve essere un metodo n� deve essere il nome di una classe
		if (entry.getType() instanceof ClassTypeNode) {
			System.out.println("Error: id " + id + " is a class type");
			System.exit(0);
		}

		if (this.entry.isMethod()) {
			System.out.println("Error: Wrong usage of method or class identifier");
			System.exit(0);
		}

		return this.entry.getType();
	}

	public String codeGeneration() {

		String getAR = ""; // recupero l'AR in cui � dichiarata la variable che sto usando
		for (int i = 0; i < nestingLevel - entry.getNestinglevel(); i++) { // differenza di nesting level tra dove sono e la dichiarazione di "id"
			getAR += "lw\n";
		}
		
		String code = "push " + entry.getOffset() + "\n" + // metto l'offset sullo stack
						"lfp\n" + 
						getAR + // risalgo la catena statica e ottengo l'indirizzo dell'AR della variabile
						"add\n" + 
						"lw\n"; // carico sullo stack il valore all'indirizzo ottenuto
		
		if (entry.getType() instanceof ArrowTypeNode) { // se di tipo ArrowTypeNode allora recupero offset-1
			code+= 	"push " + (entry.getOffset() - 1) + "\n" + // recupero l'indirizzo della funzione
					"lfp\n" + 
					getAR + 
					"add\n" + 
					"lw\n"; // carico sullo stack il valore a quell'indirizzo
		}
	
		return code;
		
	}

}
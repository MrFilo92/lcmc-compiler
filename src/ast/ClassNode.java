package ast;

import java.util.ArrayList;
import entry.STentry;
import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;
import types.*;

public class ClassNode implements Node,DecNode{
	
	private ClassTypeNode symType; //tipo messo in Symbol Table
    private ArrayList<Node> fields; //figli campi
    private ArrayList<Node> methods; //figli metodi

	public ClassNode(ClassTypeNode symType) { 
		this.symType = symType;
		this.fields = this.symType.getAllFields();	
		this.methods = this.symType.getAllMethods();
	}
	
	@Override
	public Node getSymType() {	
		return this.symType;
	}
	
	public void setSymType(ClassTypeNode symType) {
		this.symType = symType;
	}

	@Override
	public String toPrint(String indent) {
		String fieldsList = "";
		for(Node n:fields){
			fieldsList+=n.toPrint(indent+"  "); 
		}
		
		String methodsList = "";
		for(Node n:methods) {
			methodsList += n.toPrint(indent+" ");
		}
		
		return indent + "Class: " + symType.getType() + "\n" + fieldsList + methodsList + indent +
			   "Class entry:\n" + this.symType.toPrint(indent);
		
	}

	@Override
	public Node typeCheck() {
		
		// Typechecking sui figli che sono metodi della classe
		for(Node n: this.methods) {
			n.typeCheck();
		}
		
		return null; //perch� � una dichiarazione
	}

	@Override
	public String codeGeneration() { 
			
		String methodLabel;
		int methodOffset;
		ArrayList<String> dispatchTable = new ArrayList<String>(); //creo la nuova dispatch table

		for (int i = 0; i < this.methods.size(); i++){	
			// mi richiamo su ciascun metodo
			this.methods.get(i).codeGeneration();

			//recupero le etichette per tutti i metodi contenuti in methods
			methodLabel = ((MethodNode)this.methods.get(i)).getLabel();
	
			//recupero offset per ogni metodo
			methodOffset = ((MethodNode)this.methods.get(i)).getOffset();
					
		    dispatchTable.add(methodLabel);
		}
		
		FOOLlib.setDispatchTables(dispatchTable);
		
		// Devo creare la dispatch table nello heap
		// per ciascuna etichetta la memorizzo a indirizzo in $hp ed incremento $hp
		String codeDispatchTable = "";
		for(String s : dispatchTable) {
			codeDispatchTable += "push " + s + "\n" //label del metodo sullo stack
					+ "lhp\n"
					+ "sw\n" //salva nell'indirizzo puntato da HP il secondo valore che legge (s,ovvero la label del metodo)
					+ "lhp\n"
					+ "push 1\n" //effettuo la somma con 1
					+ "add\n"
					+ "shp\n"; //salva heap pointer
		}

		//metto valore di $hp sullo stack (sar� il dispatch pointer da ritornare alla fine)
		return "lhp\n"+ codeDispatchTable;

	}

}
package ast;

import interfaces.DecNode;
import interfaces.Node;

public class ParNode implements Node,DecNode {
	
	private String id;
	private Node type;
  
	public ParNode (String i, Node t) {
		this.id=i;
		this.type=t;
	}

	@Override
	public Node getSymType() {
		return this.type;
	}

	public String toPrint(String s) {
		return s+"Par:" + id +"\n"+type.toPrint(s+"  ") ; 
	}

	//non utilizzato
	public Node typeCheck(){
		return null;
	}
	
	//non utilizzato
	public String codeGeneration(){
		return "";
	}

}  
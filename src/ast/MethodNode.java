package ast;

import java.util.ArrayList;
import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;
import types.ArrowTypeNode;

public class MethodNode implements Node, DecNode {
	
	private String name;
	private Node returnType; //tipo di ritorno
	private Node symType;
	private Node exp; //corpo del metodo
	private ArrayList<Node> parlist; //lista parametri
	private ArrayList<Node> varlist; //lista dichiarazioni
	private String methodLabel; //label per code generation
	private int offset; //durante il parsing, settarlo a offset messo in Symbol Table

	public MethodNode(String name, Node returnType) {
		this.name = name;
		this.returnType = returnType;
		this.parlist = new ArrayList<>();
		this.varlist = new ArrayList<>();
	}
	
	@Override
	public Node getSymType() {	
		return this.symType;
	}
	
	public void addDec(ArrayList<Node> varlist) {
		this.varlist = varlist;
	}

	public void addBody(Node body) {
		this.exp = body;
	}

	public void addPar(Node parameter) { // metodo "addPar" che aggiunge un nodo a campo "parlist"
		this.parlist.add(parameter);
	}

	public void addSymType(Node functionType) {
		this.symType = functionType;
	}
	
	public String getLabel() {
		return this.name;
	}
	
	public void setLabel(String methodLabel) {
		this.name = methodLabel; 
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public String getMethodName() {
		return this.name;
	}

	@Override
	public String toPrint(String indent) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(indent + "  ");
		}
		;
		String declstr = "";
		for (Node dec : varlist) {
			declstr += dec.toPrint(indent + "  ");
		}
		;
		return indent + "Method:" + name + "\n" + returnType.toPrint(indent + "  ") + parlstr + declstr + exp.toPrint(indent + "  ");
	}

	@Override
	//uguale a FunNode
	public Node typeCheck() {
		for (Node dec : varlist) {
			dec.typeCheck();
		}
		;
		if (!FOOLlib.isSubtype(exp.typeCheck(), returnType)) {
			System.out.println("Incompatible value for variable");
			System.exit(0);
		}
		return null;
	}

	@Override
	public String codeGeneration() {
		
		//genera un�etichetta nuova per il suo indirizzo
		this.methodLabel = FOOLlib.freshMethodLabel();
		setLabel(methodLabel); //la setta nel suo campo 
		//genera il codice del metodo (invariato rispetto a funzioni)
		String declCode = "";
		for (Node dec : varlist) {
			declCode += dec.codeGeneration();
		}
		
		//pop delle dichiarazioni
		String popDecl = "";
		for (Node dec : varlist) {
			popDecl += "pop\n";
			// se la dichiarazione ha tipo funzione,facciamo pop anche del FP
			if (((DecNode) dec).getSymType() instanceof ArrowTypeNode) { 
				popDecl += "pop\n";
			}
		}
		
		// pop dei parametri
		String popParl = "";
		for (Node par : parlist) {
			popParl += "pop\n";
			if (((DecNode) par).getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
		}
		
		FOOLlib.putCode(methodLabel + ":\n" + "cfp\n" + // setta $fp allo $sp
				"lra\n" + // inserimento Return Address
				declCode + exp.codeGeneration() + "srv\n" + // pop del return value e memorizzazione in $rv
				popDecl + // una pop per ogni dichiarazione
				"sra\n" + // pop del Return Address e memorizzazione in $ra
				"pop\n" + // pop di AL
				popParl + "sfp\n" + // ripristino il frame pointer al valore del CL del chiamante
				"lrv\n" + // risultato della funzione sullo stack
				"lra\n" + "js\n" // salta a $ra che ho caricato sullo stack con l'istruzione precedente
		);
		
		//ritorna codice vuoto
		return "";
	}


}

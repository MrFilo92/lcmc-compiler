package ast;

import java.util.ArrayList;
import entry.STentry;
import interfaces.Node;
import lib.FOOLlib;
import types.*;

public class ClassCallNode implements Node {

	private String objectId;
	private int nestingLevel;
	private STentry entry; // STentry di ID1 (oggetto)
	private STentry methodEntry; // STentry di ID2 --> cercata nella Virtual Table della classe del tipo RefTypeNode di ID1
	private String methodId;
	private ArrayList<Node> parlist;

	public ClassCallNode(String objectId, int nestingLevel, STentry st, STentry methodEntry, String methodId,
			ArrayList<Node> parlist) {
		this.objectId = objectId;
		this.nestingLevel = nestingLevel;
		this.entry = st;
		this.methodEntry = methodEntry;
		this.methodId = methodId;
		this.parlist = new ArrayList<Node>();
		this.parlist = parlist;
	}
 
	@Override
	public String toPrint(String indent) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(indent + "  ");
		}
		;

		return indent + "ClassCall:" + objectId + "." + methodId + " at nesting level " + nestingLevel + "\n"
				+ entry.toPrint(indent + "") + indent + "Method Entry:" + "\n" + methodEntry.toPrint(indent + "") + parlstr;

	}

	@Override
	// typechecking uguale a CallNode
	public Node typeCheck() {
		ArrowTypeNode type = null;
		if (methodEntry.getType() instanceof ArrowTypeNode)
			type = (ArrowTypeNode) methodEntry.getType();
		else {
			System.out.println("Object usage of a non object identifier " + objectId);
			System.exit(0);
		}
		ArrayList<Node> p = type.getParList();
		if (!(p.size() == parlist.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + objectId + "." + methodId);
			System.exit(0);
		}
		for (int i = 0; i < parlist.size(); i++)
			if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + objectId + "."
						+ methodId);
				System.exit(0);
			}
		return type.getRet();

	}

	@Override
	public String codeGeneration() {
		
		String parCode = "";
		for (int i = parlist.size() - 1; i >= 0; i--) {
			parCode += parlist.get(i).codeGeneration();
		}

		String getAR = ""; 
		for (int i = 0; i < nestingLevel - entry.getNestinglevel(); i++) {
			getAR += "lw\n"; // differenza di nesting level tra dove sono e la dichiarazione dell'oggetto
		}
	    
		//si deve settare l'Access Link nell'AR del metodo invocato al valore ottenuto
		//poi,si recupera l'indrizzo del metodo a cui saltare(usando l�offset di ID2 nella dispatch table riferita dal dispatch pointer dell�oggetto)
		
		return  "lfp\n" + //carico sullo stack il frame pointer del chiamante 
				parCode+  //alloco i parametri
				"push "+(entry.getOffset())+"\n"+ //metto l'offset sullo stack
				"lfp\n"+ //carico il frame pointer sullo stack
				getAR + //risalgo la catena statica ottenendo il valore di ID1 dove � dichiarato l'oggetto
				"add\n" + //sommo l'offset all'indirizzo che abbiamo ottenuto
				"lw\n"+ //carico sullo stack l'object pointer
				"push "+(entry.getOffset())+"\n"+ //metto l'offset sullo stack
				"lfp\n"+//carico il frame pointer sullo stack
				getAR + //risalgo la catena statica ottenendo il valore di ID2 dove � dichiarato l'oggetto
				"add\n" + //sommo l'offset all'indirizzo raggiunto
				"lw\n"+ //metto sullo stack object pointer
				"lw\n"+ //metto sullo stack dispatch pointer 
				"push "+methodEntry.getOffset()+"\n"+ //carico offset del metodo a cui saltare
				"add\n" + //offset del metodo + dispatch pointer
				"lw\n"+ //carico su stack il metodo
				"js\n"; //infine,effettuo il salto
		
	}

}


package ast;

import interfaces.Node;
import lib.FOOLlib;
import types.BoolTypeNode;

public class IfNode implements Node {

  private Node condition;
  private Node then;
  private Node el;
  
  public IfNode (Node c, Node t, Node e) {
   this.condition=c;
   this.then=t;
   this.el=e;
  }
  
  public String toPrint(String s) {
   return s+"If\n" + condition.toPrint(s+"  ") 
                 + then.toPrint(s+"  ")   
                 + el.toPrint(s+"  ") ; 
  }

  public Node typeCheck() {
	if ( !(FOOLlib.isSubtype(condition.typeCheck(), new BoolTypeNode())) ) {
	  System.out.println("non boolean condition in if");
      System.exit(0);		
	}
	Node t= then.typeCheck();
	Node e= el.typeCheck();  
	if (FOOLlib.isSubtype(t, e))
      return e;
	if (FOOLlib.isSubtype(e, t))
	  return t;
	System.out.println("Incompatible types in then-else branches");
    System.exit(0);
    return null;
  }
  
  public String codeGeneration() {
	  String l1 = FOOLlib.freshLabel();
	  String l2 = FOOLlib.freshLabel();
	  return condition.codeGeneration()+
			 "push 1\n"+
		     "beq "+l1+"\n"+
			 el.codeGeneration()+
			 "b "+l2+"\n"+
			 l1 + ": \n"+
			 then.codeGeneration()+
	         l2 + ": \n";		  
  }

}  
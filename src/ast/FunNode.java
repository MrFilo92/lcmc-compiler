package ast;

import java.util.ArrayList;
import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;
import types.ArrowTypeNode;

public class FunNode implements Node, DecNode {

	private String id;
	private Node type;
	private Node exp;
	private Node symType; //tipo messo in symbol table
	private ArrayList<ParNode> parlist;
	private ArrayList<DecNode> declist;

	public FunNode(String i, Node t) {
		id = i;
		type = t;
		parlist = new ArrayList<ParNode>(); // campo "parlist" che � lista di Node lista di parametri
		declist = new ArrayList<DecNode>(); //lista dichiarazioni
	}

	public void addDec(ArrayList<DecNode> d) {
		declist = d;
	} 

	public void addBody(Node b) {
		exp = b;
	} 

	public void addPar(ParNode p) { // metodo "addPar" che aggiunge un nodo a campo "parlist"
		parlist.add(p);
	}

	public void addSymType(Node functionType) {
		this.symType = functionType;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}
		
		String declstr = "";
		for (Node dec : declist) {
			declstr += dec.toPrint(s + "  ");
		}
		
		return s + "Fun:" + id + "\n" + type.toPrint(s + "  ") + parlstr + declstr + exp.toPrint(s + "  ");
	}

	public Node typeCheck() {
		for (Node dec : declist) {
			dec.typeCheck();
		}
		;
		if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {
			System.out.println("Incompatible value for variable");
			System.exit(0);
		}
		return null;
	}

	public String codeGeneration() {
		String funl = FOOLlib.freshFunLabel();

		String declCode = "";
		for (Node dec : declist) {
			declCode += dec.codeGeneration();
		}
        
		//pop delle dichiarazioni
		String popDecl = "";
		for (Node dec : declist) {
			popDecl += "pop\n";
			// se la dichiarazione ha tipo funzione,facciamo pop anche del FP
			if (((DecNode) dec).getSymType() instanceof ArrowTypeNode) { 
				popDecl += "pop\n";
			}
		}

		// pop dei parametri
		String popParl = "";
		for (Node par : parlist) {
			popParl += "pop\n";
			if (((DecNode) par).getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
		}

		FOOLlib.putCode(funl + ":\n" + "cfp\n" + // setta $fp allo $sp
				"lra\n" + // inserimento Return Address
				declCode + exp.codeGeneration() + "srv\n" + // pop del return value e memorizzazione in $rv
				popDecl + // una pop per ogni dichiarazione
				"sra\n" + // pop del Return Address e memorizzazione in $ra
				"pop\n" + // pop di AL
				popParl + "sfp\n" + // ripristino il $fp al valore del CL
				"lrv\n" + // risultato della funzione sullo stack
				"lra\n" + "js\n" // salta a $ra
		);

		return "lfp\n" + "push " + funl + "\n"; // metto fp corrente ed indirizzo a cui saltare
	}

	@Override
	public Node getSymType() {
		return this.symType;
	}

}
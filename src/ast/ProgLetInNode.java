package ast;

import java.util.ArrayList;

import interfaces.DecNode;
import interfaces.Node;
import lib.*;

public class ProgLetInNode implements Node {

  private ArrayList<DecNode> declist;
  private ArrayList<DecNode> cllist;
  private Node exp;
  
  public ProgLetInNode (ArrayList<DecNode> c, ArrayList<DecNode> d,  Node e) { //campo per clllist a null
   this.declist=d;
   this.cllist = c;
   this.exp=e;
  }
   
  public String toPrint(String s) {
	 String cllstr="";
	 String declstr="";
	 for (Node cl:cllist) {cllstr+=cl.toPrint(s+ " ");}
	 for (Node dec:declist){ declstr+=dec.toPrint(s+"  "); }
     return s+"ProgLetIn\n" +  cllstr + declstr + exp.toPrint(s+"  ") ; 
  }

  public Node typeCheck() {
    for (Node cl:cllist){cl.typeCheck();}
    for (Node dec:declist){dec.typeCheck();}
    return exp.typeCheck();
  }
    
  public String codeGeneration() {
	    String code="";
	    for (DecNode cl: cllist){code+=cl.codeGeneration();};
	    for (DecNode dec: declist){code+=dec.codeGeneration();};

		return "push 0\n"+
				code+
			    exp.codeGeneration()+
			    "halt\n"+
			    FOOLlib.getCode();
  }
}  
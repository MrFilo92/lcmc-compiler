package ast;

import interfaces.DecNode;
import interfaces.Node;

public class FieldNode implements Node, DecNode {
	
	private String name;
	private Node type;

	public FieldNode(String name, Node type) {
		this.name = name;
		this.type = type;
	}
	
	@Override
	public Node getSymType() {	
		return this.type;
	}

	
	public void setSymType(Node type) {	
		this.type=type;
	}
	
	public String getFieldName() {
		return this.name;
	}

	
	@Override
	//toPrint simile a quello di ParNode
	public String toPrint(String indent) {
		
		return indent + "Field: " + name + "\n" +
		       type.toPrint(indent + " ");
	}

	@Override
	//non utilizzato
	public Node typeCheck() {		
		return null;
	}

	@Override
	//non utilizzato
	public String codeGeneration() {	
		return "";
	}

}

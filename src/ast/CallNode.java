package ast;

import java.util.ArrayList;
import entry.STentry;
import interfaces.Node;
import lib.FOOLlib;
import types.*;

public class CallNode implements Node {

  private String id;
  private int nestingLevel;
  private STentry entry;
  private ArrayList<Node> parlist = new ArrayList<Node>(); 
  
  public CallNode (String i, STentry st, ArrayList<Node> p, int nl) {
	  this.id=i;
	  this.entry=st;
	  this.parlist=p;
	  this.nestingLevel=nl;
  }
 
  
  public String toPrint(String s) { 
	 String parlstr="";
	 for (Node par:parlist){
		 parlstr+=par.toPrint(s+"  ");
	 }
	 return s+"Call: " + id + " at nestinglevel "+nestingLevel+"\n"+
		   this.entry.toPrint(s+"  ") + parlstr;
  }
  
  public Node typeCheck() {	 
	  
		 ArrowTypeNode t = null;
		 
		 //controlla se il tipo messo in symbol table � di tipo funzionale
	     if (this.entry.getType() instanceof ArrowTypeNode) {
	    	 t = (ArrowTypeNode) this.entry.getType(); 
	     }else{
	       System.out.println("Invocation of a non-function "+id);
	       System.exit(0);
	     }
	     
	     ArrayList<Node> p = t.getParList(); //lista di parametri messa in symbol table
	     if (!(p.size() == this.parlist.size())){ //se � diversa da quella passata al costruttore
	       System.out.println("Wrong number of parameters in the invocation of "+id);
	       System.exit(0);
	     } 
	     
	   //per ogni elemento della lista dei parametri controlla che il tipo sia sottotipo dell'attuale
	     for (int i=0; i<this.parlist.size(); i++) 
	       if (!(FOOLlib.isSubtype((this.parlist.get(i)).typeCheck(), p.get(i)))) {
	         System.out.println("Call node - Wrong type for "+(i+1)+"-th parameter in the invocation of "+id);
	         System.exit(0);
	       } 
	     return t.getRet();
  }
  
 public String codeGeneration() {
		 
	  String getAR=""; //recupero l'AR in cui � dichiarata la funzione che sto usando
	  String parCode=""; 
	  for (int i=parlist.size()-1; i>=0; i--)
		  parCode+=parlist.get(i).codeGeneration();

		//controllo se ID � un metodo ("isMethod" in STentry)
		 if(this.entry.isMethod()) {
			// se lo �: ritorno codice di estensione Object Oriented
			 
			 //recupero l'AR in cui � dichiarata la funzione che sto usando risalendo la catena statica
			 //differenza di nesting level +1 in modo da raggiungere la dispatch table
			for (int i=0;i<((nestingLevel-entry.getNestinglevel())+1);i++) {//aggiungo 1 alla differenza di nesting level tra dove sono e la dichiarazione di "id"
				getAR+="lw\n";
			}
	
			 return 	"lfp\n"+ //alloco sullo stack il frame pointer del chiamante
				 		parCode+
				         "lfp\n" + 
				 		 getAR + //AL: risalgo la catena statica per ottenere l'indirizzo della dispatch table dove � salvato il metodo
						 //codice per recuperare l'indirizzo a cui saltare (stesso delle variabili)
						 "push "+ entry.getOffset() +"\n"+ //metto l'offset sullo stack(indirizzo funzione a offset ID -1)
			             "lfp\n" + 
						 getAR + //risalgo la catena statica e ottengo l'indirizzo dell'AR della variabile	 
						 "add\n"+ //aggiungo l'offset del metodo all'indirizzo ottenuto
			             "lw\n"+
						 "js\n"; //eseguo il salto      		 
				 	 
		 }else{
			 
			 //recupero l'AR in cui � dichiarata la funzione che sto usando risalendo la catena statica
			  //metto sullo stack i parametri in ordine inverso
			  for (int i=0;i<nestingLevel-entry.getNestinglevel();i++) {
				  getAR+="lw\n";//differenza di nesting level tra dove sono e la dichiarazione di "id"
			  }
				
			 // se non lo �, ritorno codice di estensione Higher Order 
			//allocazione della mia parte dell'AR della funzione che sto chiamando
			 return  //allocazione della mia parte dell'AR della funzione che sto chiamando
					 "lfp\n"+ //CL:frame pointer chiamante
			         parCode + //allocazione valori parametri
			         "push " + entry.getOffset() + "\n" + //offset sullo stack
			         "lfp\n" +
			         getAR + //AL: solita risalita della catena statica
			         "add\n" + //sommo offset all'indirizzo di AR ottenuto
			         "lw\n" + //AR pi� recente dove � dichiarata la funzione
					 //codice per recuperare l'inidirizzo a cui saltare (stesso delle variabili)
					 "push "+ (entry.getOffset() - 1) +"\n"+ //metto l'offset sullo stack
		             "lfp\n" + 
					 getAR + //risalgo la catena statica e ottengo l'indirizzo dell'AR della variabile	 
					 "add\n"+
		             "lw\n"+ //carico sullo stack l'indirizzo a cui saltare
			         "js\n"; //effettuo il salto
		 }
	

	  }
    
}  
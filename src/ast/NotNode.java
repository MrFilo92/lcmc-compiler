package ast;

import interfaces.Node;
import lib.FOOLlib;
import types.BoolTypeNode;

public class NotNode implements Node {

	private Node right;

	public NotNode(Node r) {
		right = r;
	}

	public String toPrint(String s) {
		return s + "Not\n" + right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
		if(!FOOLlib.isSubtype(right.typeCheck(), new BoolTypeNode())){
			System.out.println("Non boolean in not!");
			System.exit(0);
		}

		return new BoolTypeNode();
	}

	public String codeGeneration() {
		String l1 = FOOLlib.freshLabel(); 
		String l2 = FOOLlib.freshLabel();
	    return    right.codeGeneration() +       
				  "push 1\n"+               
				  "beq "+l1+"\n"+                 //beq = salta alla label corrispondente se uguale
				  "push 1\n"+                     //b = salto incondizionato alla label
				  "b "+l2+"\n"+                      
				  l1+":\n"+                       //se troviamo true(1) dopo aver pushato 1 allora saltiamo ad l1 e pushamo
				  "push 0\n"+                     //false(0),altrimenti andiamo avanti poich� abbiamo un false in entrata e 
				  l2+":\n";				          //pushamo un 1 che corrisponde a true

	}

}



package ast;

import interfaces.Node;

public class PrintNode implements Node {

  private Node exp;
  
  public PrintNode (Node e) {
   this.exp=e;
  }
  
  public String toPrint(String s) {
   return s+"Print\n" + this.exp.toPrint(s+"  ") ;
  }

  public Node typeCheck() {
	    return exp.typeCheck();
  }
      
  public String codeGeneration() {
		return exp.codeGeneration()+"print\n";
  }

}  
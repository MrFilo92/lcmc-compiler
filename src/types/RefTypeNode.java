package types;

import interfaces.Node;

public class RefTypeNode implements Node {

	String idClass; //id classe come campo
	
	public RefTypeNode(String idClass) {
		this.idClass = idClass;
	}
	
	public String getIdClass() {
		return this.idClass;
	}
	
	public String getType() {
		return this.idClass;
	}
	
	@Override
	public String toPrint(String indent) {
		return indent+"RefTypeNode: "+this.idClass+"\n";
	}

	@Override
	public Node typeCheck() {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}



}

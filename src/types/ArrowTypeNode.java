package types;
import java.util.ArrayList;

import interfaces.Node;

public class ArrowTypeNode implements Node {

  private ArrayList<Node> parlist; //lista parametri della funzione
  private Node returnNode; //tipo di ritorno della funzione
  
  public ArrowTypeNode (ArrayList<Node> p, Node r) {
	  this.parlist = p;
	  this.returnNode = r;
  }

  public Node getRet() { 
	    return this.returnNode;
  }
	  
  public ArrayList<Node> getParList () { 
	    return this.parlist;
  }
  
  public String toPrint(String s) {
	 String parlstr="";
	 for (Node par : parlist){
		 parlstr += par.toPrint(s+"  "); //assegna ogni parametro della lista che verr� stampata
	 }
     return s+"ArrowTypeNode\n" + parlstr + returnNode.toPrint(s+"  ->") ; 
  }

  //non utilizzato
  public Node typeCheck(){
	  return null;
  } 

  //non utilizzato
  public String codeGeneration(){
	  return "";
  }

}  
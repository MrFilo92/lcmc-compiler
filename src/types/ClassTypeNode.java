package types;

import java.util.ArrayList;

import interfaces.Node;

public class ClassTypeNode implements Node{

	private String name;
	private ArrayList<Node> allFields;
	private ArrayList<Node> allMethods;

	public ClassTypeNode(String name, ArrayList<Node> allFields, ArrayList<Node> allMethods) {
		this.name = name;
		this.allFields = allFields;
		this.allMethods = allMethods;
	}
	
	public void setAllFields(ArrayList<Node> allFields) {
		this.allFields = allFields;
	}

	public void setAllMethods(ArrayList<Node> allMethods) {
		this.allMethods = allMethods;
	}

	public ArrayList<Node> getAllFields(){
		return this.allFields;
	}

	public ArrayList<Node> getAllMethods(){
		return this.allMethods;
	}
	
	public ClassTypeNode(String name) {
		this.name = name;
	}

	public String getType() {
		return this.name;
	}
	  
	@Override
	public String toPrint(String indent) {
		return indent+" ClassTypeNode: "+this.name+"\n";
	}

	@Override
	public Node typeCheck() {
		return null;
	}

	@Override
	public String codeGeneration() {
		return "";
	}

}
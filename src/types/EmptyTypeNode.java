package types;

import interfaces.Node;

public class EmptyTypeNode implements Node{

	@Override
	public String toPrint(String indent) {
		return indent+"EmptyTypeNode\n";
	}

	@Override
	public Node typeCheck() {
		return new EmptyTypeNode();
	}

	@Override
	public String codeGeneration() {
		return "";
	}


}

// Generated from FOOL.g4 by ANTLR 4.7

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
import entry.*;
import interfaces.*;
import lib.*;
import types.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COLON=1, COMMA=2, ASS=3, SEMIC=4, EQ=5, LE=6, GE=7, AND=8, NOT=9, OR=10, 
		PLUS=11, MINUS=12, TIMES=13, DIV=14, INTEGER=15, TRUE=16, FALSE=17, LPAR=18, 
		RPAR=19, CLPAR=20, CRPAR=21, DOT=22, IF=23, THEN=24, ELSE=25, PRINT=26, 
		LET=27, IN=28, VAR=29, FUN=30, CLASS=31, EXTENDS=32, NEW=33, NULL=34, 
		INT=35, BOOL=36, ARROW=37, ID=38, WHITESP=39, COMMENT=40, ERR=41;
	public static final int
		RULE_prog = 0, RULE_cllist = 1, RULE_declist = 2, RULE_hotype = 3, RULE_type = 4, 
		RULE_arrow = 5, RULE_exp = 6, RULE_term = 7, RULE_factor = 8, RULE_value = 9;
	public static final String[] ruleNames = {
		"prog", "cllist", "declist", "hotype", "type", "arrow", "exp", "term", 
		"factor", "value"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "','", "'='", "';'", "'=='", "'<='", "'>='", "'&&'", "'!'", 
		"'||'", "'+'", "'-'", "'*'", "'/'", null, "'true'", "'false'", "'('", 
		"')'", "'{'", "'}'", "'.'", "'if'", "'then'", "'else'", "'print'", "'let'", 
		"'in'", "'var'", "'fun'", "'class'", "'extends'", "'new'", "'null'", "'int'", 
		"'bool'", "'->'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COLON", "COMMA", "ASS", "SEMIC", "EQ", "LE", "GE", "AND", "NOT", 
		"OR", "PLUS", "MINUS", "TIMES", "DIV", "INTEGER", "TRUE", "FALSE", "LPAR", 
		"RPAR", "CLPAR", "CRPAR", "DOT", "IF", "THEN", "ELSE", "PRINT", "LET", 
		"IN", "VAR", "FUN", "CLASS", "EXTENDS", "NEW", "NULL", "INT", "BOOL", 
		"ARROW", "ID", "WHITESP", "COMMENT", "ERR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "FOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	private int nestingLevel = 0;
	private int globalOffset=-2;

	private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();

	//Mappa il nome della classe alla sua virtual table
	//Serve per preservare le dichiarazioni interne ad una classe una volta che il parser ha concluso la sua dichiarazione
	private HashMap<String,HashMap<String,STentry>> classTable = new HashMap<String,HashMap<String,STentry>>();

	public FOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public Node ast;
		public CllistContext c;
		public DeclistContext d;
		public ExpContext ex;
		public ExpContext e;
		public TerminalNode SEMIC() { return getToken(FOOLParser.SEMIC, 0); }
		public TerminalNode LET() { return getToken(FOOLParser.LET, 0); }
		public TerminalNode IN() { return getToken(FOOLParser.IN, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public CllistContext cllist() {
			return getRuleContext(CllistContext.class,0);
		}
		public DeclistContext declist() {
			return getRuleContext(DeclistContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				HashMap<String,STentry> hm = new HashMap<String,STentry> ();
			       symTable.add(hm);
			       boolean isClassList = false;
			       boolean isDecList = false;
			       
			setState(40);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LET:
				{
				setState(21);
				match(LET);
				setState(32);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CLASS:
					{
					setState(22);
					((ProgContext)_localctx).c = cllist();
					isClassList=true;
					setState(27);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==VAR || _la==FUN) {
						{
						setState(24);
						((ProgContext)_localctx).d = declist();
						isDecList=true;
						}
					}

					}
					break;
				case VAR:
				case FUN:
					{
					setState(29);
					((ProgContext)_localctx).d = declist();
					isDecList = true;
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(34);
				match(IN);
				setState(35);
				((ProgContext)_localctx).ex = exp();
				}
				break;
			case NOT:
			case INTEGER:
			case TRUE:
			case FALSE:
			case LPAR:
			case IF:
			case PRINT:
			case NEW:
			case NULL:
			case ID:
				{
				setState(37);
				((ProgContext)_localctx).e = exp();
				((ProgContext)_localctx).ast =  new ProgNode(((ProgContext)_localctx).e.ast);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}

			  	      	if(isClassList == true && isDecList == true){
			    			((ProgContext)_localctx).ast =  new ProgLetInNode(((ProgContext)_localctx).c.astlist, ((ProgContext)_localctx).d.astlist, ((ProgContext)_localctx).ex.ast); 
			    		}else if(isClassList == true && isDecList == false){
			    			((ProgContext)_localctx).ast =  new ProgLetInNode(((ProgContext)_localctx).c.astlist, new ArrayList<DecNode>(), ((ProgContext)_localctx).ex.ast);
			        	}else if(isClassList == false && isDecList == true){
			        		((ProgContext)_localctx).ast =  new ProgLetInNode(new ArrayList<DecNode>(), ((ProgContext)_localctx).d.astlist, ((ProgContext)_localctx).ex.ast);
			        	}else{
			        		((ProgContext)_localctx).ast =  new ProgLetInNode(new ArrayList<DecNode>(), new ArrayList<DecNode>(), ((ProgContext)_localctx).ex.ast);
			        	}  
			        	symTable.remove(nestingLevel);
			        
			setState(43);
			match(SEMIC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CllistContext extends ParserRuleContext {
		public ArrayList<DecNode> astlist;
		public Token i;
		public Token pfid;
		public TypeContext pft;
		public Token pnid;
		public TypeContext pnt;
		public Token mid;
		public TypeContext mt;
		public Token mpfid;
		public HotypeContext mpfty;
		public Token mpnid;
		public HotypeContext mpnty;
		public Token vid;
		public TypeContext vt;
		public ExpContext ve;
		public ExpContext me;
		public List<TerminalNode> CLASS() { return getTokens(FOOLParser.CLASS); }
		public TerminalNode CLASS(int i) {
			return getToken(FOOLParser.CLASS, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<TerminalNode> EXTENDS() { return getTokens(FOOLParser.EXTENDS); }
		public TerminalNode EXTENDS(int i) {
			return getToken(FOOLParser.EXTENDS, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public CllistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cllist; }
	}

	public final CllistContext cllist() throws RecognitionException {
		CllistContext _localctx = new CllistContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_cllist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((CllistContext)_localctx).astlist =  new ArrayList<DecNode>(); 
			setState(130); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(46);
				match(CLASS);
				setState(47);
				((CllistContext)_localctx).i = match(ID);

							
								HashMap<String,STentry> hm = symTable.get(0); //nome della classe mappato a una nuova STentry(recuperato a livello 0 symbol table)
								
								STentry classEntry = new STentry(nestingLevel, globalOffset--); //decremento offset ogni volta che si incontra una nuova dichiarazione di classe
								
								 if (hm.put((((CllistContext)_localctx).i!=null?((CllistContext)_localctx).i.getText():null), classEntry) != null){ 
						         	System.out.println("Class id "+(((CllistContext)_localctx).i!=null?((CllistContext)_localctx).i.getText():null)+" at line "+(((CllistContext)_localctx).i!=null?((CllistContext)_localctx).i.getLine():0)+" already declared");
						          	System.exit(0);
						      	}
				 		      	
				 		      	HashMap<String,STentry> virtualTable = new HashMap<String,STentry>(); //conterr� le informazioni della classe
				 		      	
				 		      	int offsetFields = -1; //parto da -1 e decremento
				 		      	int offsetMethods = 0; //parto da 0 ed incremento

								//la classe non eredita da altre classi
								//il tipo � un nuovo oggetto ClassTypeNode con una lista inizialmente vuota in allFields e allMethods	
					
								ArrayList<Node> allFields = new ArrayList<Node>();
								ArrayList<Node> allMethods = new ArrayList<Node>();
								
								//creo nodo di tipo classe con il nome della classe
								ClassTypeNode classType = new ClassTypeNode((((CllistContext)_localctx).i!=null?((CllistContext)_localctx).i.getText():null), allFields, allMethods);	
								
								//creo nodo classe
								ClassNode c = new ClassNode(classType);
								
								//aggiungo la classe alla lista delle classi
								_localctx.astlist.add(c); 
						 		      	
						  	
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTENDS) {
					{
					setState(49);
					match(EXTENDS);
					setState(50);
					match(ID);
					}
				}

				setState(53);
				match(LPAR);

								
								//aggiungo il tipo ClassTypeNode alla entry della classe
								classEntry.addType(classType);
								
								//setto alla classe il tipo messo in symbol table
							 	c.setSymType(classType);
							 	
							 	//nella Class Table viene aggiunto il nome della classe
								//mappato ad una nuova Virtual Table creata vuota perch� non si eredita
								classTable.put((((CllistContext)_localctx).i!=null?((CllistContext)_localctx).i.getText():null), virtualTable);
								
								//aumento il livello perch� il parser � dentro una classe
								nestingLevel++; //dentro le classi il nesting level � sempre 1
							
							 	//la SymbolTable per il livello corrispondente (livello 1) deve includere anche la VirtualTable
								symTable.add(virtualTable);  
							 
				setState(70);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(55);
					((CllistContext)_localctx).pfid = match(ID);
					setState(56);
					match(COLON);
					setState(57);
					((CllistContext)_localctx).pft = type();

								 	
								 	//primo campo del costruttore,quindi creo un nuovo campo come nodo dell'AST
									FieldNode field = new FieldNode((((CllistContext)_localctx).pfid!=null?((CllistContext)_localctx).pfid.getText():null), ((CllistContext)_localctx).pft.ast);	
									
									//recupero il contenuto di allFields
									allFields = classType.getAllFields();	
									
									//creo una nuova entry per il nodo del campo
									STentry newFieldEntry;
									
									newFieldEntry = new STentry(nestingLevel,  ((CllistContext)_localctx).pft.ast , offsetFields--);  //si parte da -1 come da layout e si decrementa
									allFields.add(field); //aggiungo il campo alla lista di tutti i campi
									classType.setAllFields(allFields);

									//aggiungo alla virtual table la entry del campo mappata al nome del campo
									virtualTable.put((((CllistContext)_localctx).pfid!=null?((CllistContext)_localctx).pfid.getText():null), newFieldEntry); 

												
								 
					setState(67);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(59);
						match(COMMA);
						setState(60);
						((CllistContext)_localctx).pnid = match(ID);
						setState(61);
						match(COLON);
						setState(62);
						((CllistContext)_localctx).pnt = type();
						 
									 	
									 		//tutti gli altri campi del costruttore	
									 		FieldNode fieldN = new FieldNode((((CllistContext)_localctx).pnid!=null?((CllistContext)_localctx).pnid.getText():null), ((CllistContext)_localctx).pnt.ast);	
									 		
											allFields = classType.getAllFields();	

											newFieldEntry = new STentry(nestingLevel, ((CllistContext)_localctx).pnt.ast, offsetFields--);
											allFields.add(fieldN);
											classType.setAllFields(allFields);

											virtualTable.put((((CllistContext)_localctx).pnid!=null?((CllistContext)_localctx).pnid.getText():null), newFieldEntry);
									
									 	
						}
						}
						setState(69);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(72);
				match(RPAR);
				setState(73);
				match(CLPAR);
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==FUN) {
					{
					{
					setState(74);
					match(FUN);
					setState(75);
					((CllistContext)_localctx).mid = match(ID);
					setState(76);
					match(COLON);
					setState(77);
					((CllistContext)_localctx).mt = type();

							         	
							         	MethodNode method = new MethodNode((((CllistContext)_localctx).mid!=null?((CllistContext)_localctx).mid.getText():null), ((CllistContext)_localctx).mt.ast); 
							         	
							         	//memorizzo il tipo dei parametri il quale andr� insieme al tipo di ritorno
					                    //a comporre il tipo complessivo del metodo
					                    
					                    ArrayList<Node> parTypes = new ArrayList<Node>();
										ArrayList<Node> varTypes = new ArrayList<Node>(); //lista di variabili (dichiarazioni non funzionali)
										
										//incremento il nestingLevel dentro lo scope del metodo
										nestingLevel++;
										
										//creare una nuova hashmap per il metodo e la aggiungo alla symbol table
										HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
										symTable.add(hmn);
										
										int parOffset = 1; //offset dei parametri(iniziano da 1 ed incrementano)
										int varOffset = -2; //dichiarazioni da -2 e decremento (ad offset 0 si trova l'access link mentre al -1 il RA)
										
										STentry newMethodEntry;
										
										//recupero l'array di allMethods
										allMethods = classType.getAllMethods();	
					      		         
					      		        //inserisco nuovo metodo        
										newMethodEntry = new STentry(nestingLevel, ((CllistContext)_localctx).mt.ast, offsetMethods++); //offset dei metodi incrementato ogni nuova dichiarazione di metodo
										newMethodEntry.setMethod();//setto come metodo per distinguerlo dai campi
										method.setOffset(offsetMethods); //durante il parsing viene settato il campo offset al metodo
										allMethods.add(method); //aggiungo il nuovo metodo alla lista dei metodi
										classType.setAllMethods(allMethods);//lo metto in classtypenode
																

						         	
					setState(79);
					match(LPAR);
					setState(95);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(80);
						((CllistContext)_localctx).mpfid = match(ID);
						setState(81);
						match(COLON);
						setState(82);
						((CllistContext)_localctx).mpfty = hotype();
						 //tipi funzionali
								         		
												ParNode firstPar = new ParNode((((CllistContext)_localctx).mpfid!=null?((CllistContext)_localctx).mpfid.getText():null),((CllistContext)_localctx).mpfty.ast); 
												method.addPar(firstPar); //aggiungo il parametro al metodo
												parTypes.add(((CllistContext)_localctx).mpfty.ast); //aggiungo il parametro alla lista dei parametri
										         		
												//nel caso in cui ci siano parametri funzionali devo riservare due spazi
												if(((CllistContext)_localctx).mpfty.ast instanceof ArrowTypeNode){
													parOffset++;
												}
												
												//verifico se il primo parametro � gi� presente nella symbol table
												if (hmn.put((((CllistContext)_localctx).mpfid!=null?((CllistContext)_localctx).mpfid.getText():null), new STentry(nestingLevel, ((CllistContext)_localctx).mpfty.ast, parOffset++)) != null){
													System.out.println("Parameter id "+(((CllistContext)_localctx).mpfid!=null?((CllistContext)_localctx).mpfid.getText():null)+" at line "+(((CllistContext)_localctx).mpfid!=null?((CllistContext)_localctx).mpfid.getLine():0)+" already declared");
													System.exit(0);
												}
												
							         		
						setState(92);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(84);
							match(COMMA);
							setState(85);
							((CllistContext)_localctx).mpnid = match(ID);
							setState(86);
							match(COLON);
							setState(87);
							((CllistContext)_localctx).mpnty = hotype();
							 //parametri successivi
								  
									         			if(((CllistContext)_localctx).mpnty.ast instanceof ArrowTypeNode){ //se il parametro � funzionale, deve occupare due spazi  
															parOffset++;                 
												        }		    	
													    ParNode nextPar = new ParNode((((CllistContext)_localctx).mpnid!=null?((CllistContext)_localctx).mpnid.getText():null), ((CllistContext)_localctx).mpnty.ast);
													    method.addPar(nextPar); //aggiungo il paramentro al metodo
													    parTypes.add(((CllistContext)_localctx).mpnty.ast);	

													    if (hmn.put((((CllistContext)_localctx).mpnid!=null?((CllistContext)_localctx).mpnid.getText():null),new STentry(nestingLevel, ((CllistContext)_localctx).mpnty.ast, parOffset++)) != null){
													    	System.out.println("Parameter id "+(((CllistContext)_localctx).mpnid!=null?((CllistContext)_localctx).mpnid.getText():null)+" at line "+(((CllistContext)_localctx).mpnid!=null?((CllistContext)_localctx).mpnid.getLine():0)+" already declared");
													     	System.exit(0);
												     	}
								     		  		
							}
							}
							setState(94);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(97);
					match(RPAR);
					setState(115);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(98);
						match(LET);
						setState(110);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==VAR) {
							{
							{
							setState(99);
							match(VAR);
							setState(100);
							((CllistContext)_localctx).vid = match(ID);
							setState(101);
							match(COLON);
							setState(102);
							((CllistContext)_localctx).vt = type();
							setState(103);
							match(ASS);
							setState(104);
							((CllistContext)_localctx).ve = exp();
							setState(105);
							match(SEMIC);
							 //le variabili possono esserci o non esserci
												          
								     					 	VarNode var = new VarNode((((CllistContext)_localctx).vid!=null?((CllistContext)_localctx).vid.getText():null), ((CllistContext)_localctx).vt.ast, ((CllistContext)_localctx).ve.ast);  
								     					 	varTypes.add(var); 
							                             
															if (hmn.put((((CllistContext)_localctx).vid!=null?((CllistContext)_localctx).vid.getText():null), new STentry(nestingLevel, ((CllistContext)_localctx).vt.ast, varOffset--)) != null){
														     	System.out.println("Var id "+(((CllistContext)_localctx).vid!=null?((CllistContext)_localctx).vid.getText():null)+" at line "+(((CllistContext)_localctx).vid!=null?((CllistContext)_localctx).vid.getLine():0)+" already declared");
														      	System.exit(0);
														  	}  
														  	
							     					 	
							}
							}
							setState(112);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						 
						 					 		//aggiungo lista di dichiarazioni al metodo
						 					 		method.addDec(varTypes);
						 					 		
										 		
						setState(114);
						match(IN);
						}
					}

						
					 					 		ArrowTypeNode methodType = new ArrowTypeNode(parTypes,((CllistContext)_localctx).mt.ast); //nodo che rappresenta il tipo funzionale del metodo
					          					newMethodEntry.addType(methodType); //aggiungo il tipo al metodo
												virtualTable.put((((CllistContext)_localctx).mid!=null?((CllistContext)_localctx).mid.getText():null), newMethodEntry);
					 					 	
					setState(118);
					((CllistContext)_localctx).me = exp();
					method.addBody(((CllistContext)_localctx).me.ast);
					setState(120);
					match(SEMIC);

								       	symTable.remove(nestingLevel--);//fine scope metodo, rimuovo il livello corrente symbol table
								       
					}
					}
					setState(127);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(128);
				match(CRPAR);
				 //All�uscita dalla dichiarazione della classe rimuovo livello corrente symbol table
						      	 symTable.remove(nestingLevel--);
						      
				}
				}
				setState(132); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CLASS );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclistContext extends ParserRuleContext {
		public ArrayList<DecNode> astlist;
		public Token i;
		public HotypeContext ho;
		public ExpContext e;
		public TypeContext t;
		public Token fid;
		public HotypeContext fty;
		public Token id;
		public HotypeContext ty;
		public DeclistContext d;
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<DeclistContext> declist() {
			return getRuleContexts(DeclistContext.class);
		}
		public DeclistContext declist(int i) {
			return getRuleContext(DeclistContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public DeclistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declist; }
	}

	public final DeclistContext declist() throws RecognitionException {
		DeclistContext _localctx = new DeclistContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((DeclistContext)_localctx).astlist =  new ArrayList<DecNode>();
				    int offset=-2;
				    if(nestingLevel==0){
				    	offset = globalOffset;
				    }
			      
			setState(183); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(179);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case VAR:
					{
					setState(135);
					match(VAR);
					setState(136);
					((DeclistContext)_localctx).i = match(ID);
					setState(137);
					match(COLON);
					setState(138);
					((DeclistContext)_localctx).ho = hotype();
					setState(139);
					match(ASS);
					setState(140);
					((DeclistContext)_localctx).e = exp();

						  		 VarNode v = new VarNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),((DeclistContext)_localctx).ho.ast,((DeclistContext)_localctx).e.ast);  
					             _localctx.astlist.add(v);                                 
					             HashMap<String,STentry> hm = symTable.get(nestingLevel);
					             if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null), new STentry(nestingLevel,((DeclistContext)_localctx).ho.ast,offset--)) != null){
						             	System.out.println("Var id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
						              	System.exit(0);
					              	}
					              	
					           		//con higher order offset decrementato 2 volte (per i tipi funzionali)
						           if(((DeclistContext)_localctx).ho.ast instanceof ArrowTypeNode){
						           	   offset--;
						           }  
					    		
					}
					break;
				case FUN:
					{
					setState(143);
					match(FUN);
					setState(144);
					((DeclistContext)_localctx).i = match(ID);
					setState(145);
					match(COLON);
					setState(146);
					((DeclistContext)_localctx).t = type();
					 //inserimento di ID nella symbol table
						           FunNode f = new FunNode((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),((DeclistContext)_localctx).t.ast); //nuovo nodo funzione
						           _localctx.astlist.add(f);                              
						           HashMap<String,STentry> hm = symTable.get(nestingLevel);
						           STentry entry = new STentry(nestingLevel,offset--);  
						           offset--;
						           if (hm.put((((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null),entry) != null){
						               	System.out.println("Fun id "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getText():null)+" at line "+(((DeclistContext)_localctx).i!=null?((DeclistContext)_localctx).i.getLine():0)+" already declared");
						                System.exit(0);
						            }
						            //creare una nuova hashmap per la symTable
						            nestingLevel++; //scope della funzione
						            HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
						            symTable.add(hmn);
					            
					setState(148);
					match(LPAR);

					            	 ArrayList<Node> parTypes = new ArrayList<Node>();
					          	     int paroffset=1;
					            
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(150);
						((DeclistContext)_localctx).fid = match(ID);
						setState(151);
						match(COLON);
						setState(152);
						((DeclistContext)_localctx).fty = hotype();

						            	parTypes.add(((DeclistContext)_localctx).fty.ast);
						                ParNode fpar = new ParNode((((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null),((DeclistContext)_localctx).fty.ast); //creo nodo ParNode
						                f.addPar(fpar);                                 //lo attacco al FunNode con addPar
										//nel caso in cui ci siano parametri funzionali devo riservare due spazi
										if(((DeclistContext)_localctx).fty.ast instanceof ArrowTypeNode){
											paroffset++;
										}
										if (hmn.put((((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null),new STentry(nestingLevel,((DeclistContext)_localctx).fty.ast,paroffset++)) != null  ){ //aggiungo dichiarazione a hmn
											System.out.println("Parameter id "+(((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getText():null)+" at line "+(((DeclistContext)_localctx).fid!=null?((DeclistContext)_localctx).fid.getLine():0)+" already declared");
											System.exit(0);
										}
									
						setState(162);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(154);
							match(COMMA);
							setState(155);
							((DeclistContext)_localctx).id = match(ID);
							setState(156);
							match(COLON);
							setState(157);
							((DeclistContext)_localctx).ty = hotype();

											if(((DeclistContext)_localctx).ty.ast instanceof ArrowTypeNode){ //se il parametro � funzionale, deve occupare due spazi  
												paroffset++;                 
									        }				    	
										    parTypes.add(((DeclistContext)_localctx).ty.ast);
										    ParNode par = new ParNode((((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null),((DeclistContext)_localctx).ty.ast);
										    f.addPar(par);
										    if ( hmn.put((((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null),new STentry(nestingLevel,((DeclistContext)_localctx).ty.ast,paroffset++)) != null  ){
										    	System.out.println("Parameter id "+(((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getText():null)+" at line "+(((DeclistContext)_localctx).id!=null?((DeclistContext)_localctx).id.getLine():0)+" already declared");
										     	System.exit(0);
									     	}
									    
							}
							}
							setState(164);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(167);
					match(RPAR);

					              	entry.addType(new ArrowTypeNode(parTypes,((DeclistContext)_localctx).t.ast));
					              	// aggiungo il tipo anche al FunNode
					              	f.addSymType(new ArrowTypeNode(parTypes,((DeclistContext)_localctx).t.ast));
					              
					setState(174);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(169);
						match(LET);
						setState(170);
						((DeclistContext)_localctx).d = declist();
						setState(171);
						match(IN);
						f.addDec(((DeclistContext)_localctx).d.astlist);
						}
					}

					setState(176);
					((DeclistContext)_localctx).e = exp();

					              	f.addBody(((DeclistContext)_localctx).e.ast);
					               //rimuovere la hashmap corrente poich� esco dallo scope               
					               symTable.remove(nestingLevel--);    
					              
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(181);
				match(SEMIC);
				}
				}
				setState(185); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VAR || _la==FUN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HotypeContext extends ParserRuleContext {
		public Node ast;
		public TypeContext t;
		public ArrowContext a;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArrowContext arrow() {
			return getRuleContext(ArrowContext.class,0);
		}
		public HotypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hotype; }
	}

	public final HotypeContext hotype() throws RecognitionException {
		HotypeContext _localctx = new HotypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_hotype);
		try {
			setState(193);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
			case BOOL:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(187);
				((HotypeContext)_localctx).t = type();
				((HotypeContext)_localctx).ast = ((HotypeContext)_localctx).t.ast;
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(190);
				((HotypeContext)_localctx).a = arrow();
				((HotypeContext)_localctx).ast = ((HotypeContext)_localctx).a.ast;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Node ast;
		public Token id;
		public TerminalNode INT() { return getToken(FOOLParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(FOOLParser.BOOL, 0); }
		public TerminalNode ID() { return getToken(FOOLParser.ID, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type);
		try {
			setState(201);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(195);
				match(INT);
				((TypeContext)_localctx).ast = new IntTypeNode();
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(197);
				match(BOOL);
				((TypeContext)_localctx).ast = new BoolTypeNode();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 3);
				{
				setState(199);
				((TypeContext)_localctx).id = match(ID);
				((TypeContext)_localctx).ast = new RefTypeNode((((TypeContext)_localctx).id!=null?((TypeContext)_localctx).id.getText():null));
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrowContext extends ParserRuleContext {
		public Node ast;
		public HotypeContext ho;
		public TypeContext t;
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public TerminalNode ARROW() { return getToken(FOOLParser.ARROW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public ArrowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrow; }
	}

	public final ArrowContext arrow() throws RecognitionException {
		ArrowContext _localctx = new ArrowContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_arrow);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			ArrayList<Node> parList = new ArrayList<Node>();
			setState(204);
			match(LPAR);
			setState(216);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << INT) | (1L << BOOL) | (1L << ID))) != 0)) {
				{
				setState(205);
				((ArrowContext)_localctx).ho = hotype();
				parList.add(((ArrowContext)_localctx).ho.ast);
				setState(213);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(207);
					match(COMMA);
					setState(208);
					((ArrowContext)_localctx).ho = hotype();
					parList.add(((ArrowContext)_localctx).ho.ast);
					}
					}
					setState(215);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(218);
			match(RPAR);
			setState(219);
			match(ARROW);
			setState(220);
			((ArrowContext)_localctx).t = type();
			((ArrowContext)_localctx).ast =  new ArrowTypeNode(parList,((ArrowContext)_localctx).t.ast);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public Node ast;
		public TermContext f;
		public TermContext l;
		public TermContext l2;
		public TermContext l3;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(FOOLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FOOLParser.PLUS, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(FOOLParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(FOOLParser.MINUS, i);
		}
		public List<TerminalNode> OR() { return getTokens(FOOLParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(FOOLParser.OR, i);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			((ExpContext)_localctx).f = term();
			((ExpContext)_localctx).ast =  ((ExpContext)_localctx).f.ast;
			setState(239);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OR) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				setState(237);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS:
					{
					setState(225);
					match(PLUS);
					setState(226);
					((ExpContext)_localctx).l = term();
					((ExpContext)_localctx).ast =  new PlusNode (_localctx.ast,((ExpContext)_localctx).l.ast);
					}
					break;
				case MINUS:
					{
					setState(229);
					match(MINUS);
					setState(230);
					((ExpContext)_localctx).l2 = term();
					((ExpContext)_localctx).ast =  new MinusNode (_localctx.ast,((ExpContext)_localctx).l2.ast);
					}
					break;
				case OR:
					{
					setState(233);
					match(OR);
					setState(234);
					((ExpContext)_localctx).l3 = term();
					((ExpContext)_localctx).ast =  new OrNode (_localctx.ast,((ExpContext)_localctx).l3.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(241);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public Node ast;
		public FactorContext f;
		public FactorContext l;
		public FactorContext l2;
		public FactorContext l3;
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<TerminalNode> TIMES() { return getTokens(FOOLParser.TIMES); }
		public TerminalNode TIMES(int i) {
			return getToken(FOOLParser.TIMES, i);
		}
		public List<TerminalNode> DIV() { return getTokens(FOOLParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(FOOLParser.DIV, i);
		}
		public List<TerminalNode> AND() { return getTokens(FOOLParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(FOOLParser.AND, i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			((TermContext)_localctx).f = factor();
			((TermContext)_localctx).ast =  ((TermContext)_localctx).f.ast;
			setState(258);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AND) | (1L << TIMES) | (1L << DIV))) != 0)) {
				{
				setState(256);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TIMES:
					{
					setState(244);
					match(TIMES);
					setState(245);
					((TermContext)_localctx).l = factor();
					((TermContext)_localctx).ast =  new MultNode (_localctx.ast,((TermContext)_localctx).l.ast);
					}
					break;
				case DIV:
					{
					setState(248);
					match(DIV);
					setState(249);
					((TermContext)_localctx).l2 = factor();
					((TermContext)_localctx).ast =  new DivNode (_localctx.ast,((TermContext)_localctx).l2.ast);
					}
					break;
				case AND:
					{
					setState(252);
					match(AND);
					setState(253);
					((TermContext)_localctx).l3 = factor();
					((TermContext)_localctx).ast =  new AndNode (_localctx.ast,((TermContext)_localctx).l3.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(260);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public Node ast;
		public ValueContext f;
		public ValueContext l;
		public ValueContext l2;
		public ValueContext l3;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> EQ() { return getTokens(FOOLParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(FOOLParser.EQ, i);
		}
		public List<TerminalNode> LE() { return getTokens(FOOLParser.LE); }
		public TerminalNode LE(int i) {
			return getToken(FOOLParser.LE, i);
		}
		public List<TerminalNode> GE() { return getTokens(FOOLParser.GE); }
		public TerminalNode GE(int i) {
			return getToken(FOOLParser.GE, i);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			((FactorContext)_localctx).f = value();
			((FactorContext)_localctx).ast =  ((FactorContext)_localctx).f.ast;
			setState(277);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << LE) | (1L << GE))) != 0)) {
				{
				setState(275);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQ:
					{
					setState(263);
					match(EQ);
					setState(264);
					((FactorContext)_localctx).l = value();
					((FactorContext)_localctx).ast =  new EqualNode(_localctx.ast,((FactorContext)_localctx).l.ast);
					}
					break;
				case LE:
					{
					setState(267);
					match(LE);
					setState(268);
					((FactorContext)_localctx).l2 = value();
					((FactorContext)_localctx).ast =  new LesserEqualNode(_localctx.ast,((FactorContext)_localctx).l2.ast);
					}
					break;
				case GE:
					{
					setState(271);
					match(GE);
					setState(272);
					((FactorContext)_localctx).l3 = value();
					((FactorContext)_localctx).ast =  new GreaterEqualNode(_localctx.ast,((FactorContext)_localctx).l3.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(279);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public Node ast;
		public Token n;
		public Token nid;
		public ExpContext fne;
		public ExpContext nne;
		public ExpContext x;
		public ExpContext y;
		public ExpContext z;
		public ExpContext e;
		public Token i;
		public ExpContext a;
		public Token mi;
		public ExpContext mfe;
		public ExpContext mne;
		public TerminalNode INTEGER() { return getToken(FOOLParser.INTEGER, 0); }
		public TerminalNode TRUE() { return getToken(FOOLParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(FOOLParser.FALSE, 0); }
		public TerminalNode NULL() { return getToken(FOOLParser.NULL, 0); }
		public TerminalNode NEW() { return getToken(FOOLParser.NEW, 0); }
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public TerminalNode IF() { return getToken(FOOLParser.IF, 0); }
		public TerminalNode THEN() { return getToken(FOOLParser.THEN, 0); }
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public TerminalNode ELSE() { return getToken(FOOLParser.ELSE, 0); }
		public TerminalNode NOT() { return getToken(FOOLParser.NOT, 0); }
		public TerminalNode PRINT() { return getToken(FOOLParser.PRINT, 0); }
		public TerminalNode DOT() { return getToken(FOOLParser.DOT, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_value);
		int _la;
		try {
			setState(377);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
				enterOuterAlt(_localctx, 1);
				{
				setState(280);
				((ValueContext)_localctx).n = match(INTEGER);
				((ValueContext)_localctx).ast =  new IntNode(Integer.parseInt((((ValueContext)_localctx).n!=null?((ValueContext)_localctx).n.getText():null)));
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(282);
				match(TRUE);
				((ValueContext)_localctx).ast =  new BoolNode(true);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 3);
				{
				setState(284);
				match(FALSE);
				((ValueContext)_localctx).ast =  new BoolNode(false);
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 4);
				{
				setState(286);
				match(NULL);
				((ValueContext)_localctx).ast =  new EmptyNode();
				}
				break;
			case NEW:
				enterOuterAlt(_localctx, 5);
				{
				setState(288);
				match(NEW);
				setState(289);
				((ValueContext)_localctx).nid = match(ID);
				setState(290);
				match(LPAR);

					    	ArrayList<Node> parList = new ArrayList<Node>(); //creo lista parametri
					    	STentry entry = (symTable.get(0)).get((((ValueContext)_localctx).nid!=null?((ValueContext)_localctx).nid.getText():null)); //STentry presa direttamente da livello 0 della Symbol Table
					    	if(classTable.get((((ValueContext)_localctx).nid!=null?((ValueContext)_localctx).nid.getText():null)) == null || entry == null){ //se non � in classTable o � nullo
					    		System.out.println("Error: class "+(((ValueContext)_localctx).nid!=null?((ValueContext)_localctx).nid.getText():null) +" at line "+ (((ValueContext)_localctx).nid!=null?((ValueContext)_localctx).nid.getLine():0) +" not declared!");
					          	System.exit(0);
							}
				    	
				setState(303);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
					{
					setState(292);
					((ValueContext)_localctx).fne = exp();
					parList.add(((ValueContext)_localctx).fne.ast);
					setState(300);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(294);
						match(COMMA);
						setState(295);
						((ValueContext)_localctx).nne = exp();
						parList.add(((ValueContext)_localctx).nne.ast);
						}
						}
						setState(302);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(305);
				match(RPAR);
				((ValueContext)_localctx).ast =  new NewNode((((ValueContext)_localctx).nid!=null?((ValueContext)_localctx).nid.getText():null),entry,parList);
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 6);
				{
				setState(307);
				match(IF);
				setState(308);
				((ValueContext)_localctx).x = exp();
				setState(309);
				match(THEN);
				setState(310);
				match(CLPAR);
				setState(311);
				((ValueContext)_localctx).y = exp();
				setState(312);
				match(CRPAR);
				setState(313);
				match(ELSE);
				setState(314);
				match(CLPAR);
				setState(315);
				((ValueContext)_localctx).z = exp();
				setState(316);
				match(CRPAR);
				((ValueContext)_localctx).ast =  new IfNode(((ValueContext)_localctx).x.ast,((ValueContext)_localctx).y.ast,((ValueContext)_localctx).z.ast);
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 7);
				{
				setState(319);
				match(NOT);
				setState(320);
				match(LPAR);
				setState(321);
				((ValueContext)_localctx).e = exp();
				setState(322);
				match(RPAR);
				((ValueContext)_localctx).ast =  new NotNode(((ValueContext)_localctx).e.ast);
				}
				break;
			case PRINT:
				enterOuterAlt(_localctx, 8);
				{
				setState(325);
				match(PRINT);
				setState(326);
				match(LPAR);
				setState(327);
				((ValueContext)_localctx).e = exp();
				setState(328);
				match(RPAR);
				((ValueContext)_localctx).ast =  new PrintNode(((ValueContext)_localctx).e.ast);
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 9);
				{
				setState(331);
				match(LPAR);
				setState(332);
				((ValueContext)_localctx).e = exp();
				setState(333);
				match(RPAR);
				((ValueContext)_localctx).ast =  ((ValueContext)_localctx).e.ast;
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 10);
				{
				setState(336);
				((ValueContext)_localctx).i = match(ID);

						//cercare la dichiarazione
				       int j = nestingLevel;
				       STentry entry = null; 
				       while (j>=0 && entry==null){
				       	entry = (symTable.get(j--)).get((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null)); //recupero la entry decrementando il nestring level
				       }
				       if(entry == null){
				       	System.out.println("Error: id "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null)+" at line "+(((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getLine():0)+" not declared");
				       	System.exit(0);
				       }          
				    	((ValueContext)_localctx).ast =  new IdNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null),entry,nestingLevel);
				setState(375);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LPAR:
					{
					setState(338);
					match(LPAR);
					ArrayList<Node> pList = new ArrayList<Node>();
					setState(351);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
						{
						setState(340);
						((ValueContext)_localctx).a = exp();
						pList.add(((ValueContext)_localctx).a.ast);
						setState(348);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(342);
							match(COMMA);
							setState(343);
							((ValueContext)_localctx).a = exp();
							pList.add(((ValueContext)_localctx).a.ast);
							}
							}
							setState(350);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(353);
					match(RPAR);
					((ValueContext)_localctx).ast =  new CallNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null),entry,pList,nestingLevel);
					}
					break;
				case DOT:
					{
					setState(355);
					match(DOT);
					setState(356);
					((ValueContext)_localctx).mi = match(ID);

								//controllo che la entry sia i tipo RefTypeNode
						    	if(!(entry.getType() instanceof RefTypeNode)){
							       	System.out.println("Error: id "+(((ValueContext)_localctx).mi!=null?((ValueContext)_localctx).mi.getText():null)+" at line "+(((ValueContext)_localctx).mi!=null?((ValueContext)_localctx).mi.getLine():0)+" is not an object");
					       			System.exit(0);
						    	}	
						    	
						    	String classId = ((RefTypeNode) entry.getType()).getIdClass(); //tipo della classe
						    	STentry methodEntry = classTable.get(classId).get((((ValueContext)_localctx).mi!=null?((ValueContext)_localctx).mi.getText():null)); //recupero il metodo

						    	if(methodEntry == null){
							       	System.out.println("Error: Method "+(((ValueContext)_localctx).mi!=null?((ValueContext)_localctx).mi.getText():null)+" at line "+(((ValueContext)_localctx).mi!=null?((ValueContext)_localctx).mi.getLine():0)+" not declared");
					       			System.exit(0);	    		
						    	}
						    	
						  	  
					setState(358);
					match(LPAR);
					ArrayList<Node> parList = new ArrayList<Node>();
					setState(371);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << INTEGER) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << ID))) != 0)) {
						{
						setState(360);
						((ValueContext)_localctx).mfe = exp();
						parList.add(((ValueContext)_localctx).mfe.ast);
						setState(368);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(362);
							match(COMMA);
							setState(363);
							((ValueContext)_localctx).mne = exp();
							parList.add(((ValueContext)_localctx).mne.ast);
							}
							}
							setState(370);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(373);
					match(RPAR);
					((ValueContext)_localctx).ast =  new ClassCallNode((((ValueContext)_localctx).i!=null?((ValueContext)_localctx).i.getText():null), nestingLevel, entry, methodEntry, classId, parList);
					}
					break;
				case COMMA:
				case SEMIC:
				case EQ:
				case LE:
				case GE:
				case AND:
				case OR:
				case PLUS:
				case MINUS:
				case TIMES:
				case DIV:
				case RPAR:
				case CRPAR:
				case THEN:
					break;
				default:
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u017e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\36\n\2\3\2\3\2\3\2\5\2#\n\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\5\2+\n\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3\66"+
		"\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3D\n\3\f\3\16\3"+
		"G\13\3\5\3I\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\7\3]\n\3\f\3\16\3`\13\3\5\3b\n\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3o\n\3\f\3\16\3r\13\3\3\3\3\3\5\3v\n\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\7\3~\n\3\f\3\16\3\u0081\13\3\3\3\3\3\6\3\u0085"+
		"\n\3\r\3\16\3\u0086\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4\u00a3\n\4\f"+
		"\4\16\4\u00a6\13\4\5\4\u00a8\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\u00b1"+
		"\n\4\3\4\3\4\3\4\5\4\u00b6\n\4\3\4\3\4\6\4\u00ba\n\4\r\4\16\4\u00bb\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\5\5\u00c4\n\5\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00cc"+
		"\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u00d6\n\7\f\7\16\7\u00d9\13\7"+
		"\5\7\u00db\n\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\7\b\u00f0\n\b\f\b\16\b\u00f3\13\b\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u0103\n\t\f\t\16\t\u0106"+
		"\13\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\n\u0116"+
		"\n\n\f\n\16\n\u0119\13\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u012d\n\13\f\13\16"+
		"\13\u0130\13\13\5\13\u0132\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\7\13\u015d\n\13\f\13\16\13\u0160\13\13\5\13"+
		"\u0162\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\7\13\u0171\n\13\f\13\16\13\u0174\13\13\5\13\u0176\n\13\3\13\3\13"+
		"\5\13\u017a\n\13\5\13\u017c\n\13\3\13\2\2\f\2\4\6\b\n\f\16\20\22\24\2"+
		"\2\2\u01a3\2\26\3\2\2\2\4/\3\2\2\2\6\u0088\3\2\2\2\b\u00c3\3\2\2\2\n\u00cb"+
		"\3\2\2\2\f\u00cd\3\2\2\2\16\u00e1\3\2\2\2\20\u00f4\3\2\2\2\22\u0107\3"+
		"\2\2\2\24\u017b\3\2\2\2\26*\b\2\1\2\27\"\7\35\2\2\30\31\5\4\3\2\31\35"+
		"\b\2\1\2\32\33\5\6\4\2\33\34\b\2\1\2\34\36\3\2\2\2\35\32\3\2\2\2\35\36"+
		"\3\2\2\2\36#\3\2\2\2\37 \5\6\4\2 !\b\2\1\2!#\3\2\2\2\"\30\3\2\2\2\"\37"+
		"\3\2\2\2#$\3\2\2\2$%\7\36\2\2%&\5\16\b\2&+\3\2\2\2\'(\5\16\b\2()\b\2\1"+
		"\2)+\3\2\2\2*\27\3\2\2\2*\'\3\2\2\2+,\3\2\2\2,-\b\2\1\2-.\7\6\2\2.\3\3"+
		"\2\2\2/\u0084\b\3\1\2\60\61\7!\2\2\61\62\7(\2\2\62\65\b\3\1\2\63\64\7"+
		"\"\2\2\64\66\7(\2\2\65\63\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2\678\7\24"+
		"\2\28H\b\3\1\29:\7(\2\2:;\7\3\2\2;<\5\n\6\2<E\b\3\1\2=>\7\4\2\2>?\7(\2"+
		"\2?@\7\3\2\2@A\5\n\6\2AB\b\3\1\2BD\3\2\2\2C=\3\2\2\2DG\3\2\2\2EC\3\2\2"+
		"\2EF\3\2\2\2FI\3\2\2\2GE\3\2\2\2H9\3\2\2\2HI\3\2\2\2IJ\3\2\2\2JK\7\25"+
		"\2\2K\177\7\26\2\2LM\7 \2\2MN\7(\2\2NO\7\3\2\2OP\5\n\6\2PQ\b\3\1\2Qa\7"+
		"\24\2\2RS\7(\2\2ST\7\3\2\2TU\5\b\5\2U^\b\3\1\2VW\7\4\2\2WX\7(\2\2XY\7"+
		"\3\2\2YZ\5\b\5\2Z[\b\3\1\2[]\3\2\2\2\\V\3\2\2\2]`\3\2\2\2^\\\3\2\2\2^"+
		"_\3\2\2\2_b\3\2\2\2`^\3\2\2\2aR\3\2\2\2ab\3\2\2\2bc\3\2\2\2cu\7\25\2\2"+
		"dp\7\35\2\2ef\7\37\2\2fg\7(\2\2gh\7\3\2\2hi\5\n\6\2ij\7\5\2\2jk\5\16\b"+
		"\2kl\7\6\2\2lm\b\3\1\2mo\3\2\2\2ne\3\2\2\2or\3\2\2\2pn\3\2\2\2pq\3\2\2"+
		"\2qs\3\2\2\2rp\3\2\2\2st\b\3\1\2tv\7\36\2\2ud\3\2\2\2uv\3\2\2\2vw\3\2"+
		"\2\2wx\b\3\1\2xy\5\16\b\2yz\b\3\1\2z{\7\6\2\2{|\b\3\1\2|~\3\2\2\2}L\3"+
		"\2\2\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082\3\2\2"+
		"\2\u0081\177\3\2\2\2\u0082\u0083\7\27\2\2\u0083\u0085\b\3\1\2\u0084\60"+
		"\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0087\3\2\2\2\u0087"+
		"\5\3\2\2\2\u0088\u00b9\b\4\1\2\u0089\u008a\7\37\2\2\u008a\u008b\7(\2\2"+
		"\u008b\u008c\7\3\2\2\u008c\u008d\5\b\5\2\u008d\u008e\7\5\2\2\u008e\u008f"+
		"\5\16\b\2\u008f\u0090\b\4\1\2\u0090\u00b6\3\2\2\2\u0091\u0092\7 \2\2\u0092"+
		"\u0093\7(\2\2\u0093\u0094\7\3\2\2\u0094\u0095\5\n\6\2\u0095\u0096\b\4"+
		"\1\2\u0096\u0097\7\24\2\2\u0097\u00a7\b\4\1\2\u0098\u0099\7(\2\2\u0099"+
		"\u009a\7\3\2\2\u009a\u009b\5\b\5\2\u009b\u00a4\b\4\1\2\u009c\u009d\7\4"+
		"\2\2\u009d\u009e\7(\2\2\u009e\u009f\7\3\2\2\u009f\u00a0\5\b\5\2\u00a0"+
		"\u00a1\b\4\1\2\u00a1\u00a3\3\2\2\2\u00a2\u009c\3\2\2\2\u00a3\u00a6\3\2"+
		"\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a8\3\2\2\2\u00a6"+
		"\u00a4\3\2\2\2\u00a7\u0098\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\3\2"+
		"\2\2\u00a9\u00aa\7\25\2\2\u00aa\u00b0\b\4\1\2\u00ab\u00ac\7\35\2\2\u00ac"+
		"\u00ad\5\6\4\2\u00ad\u00ae\7\36\2\2\u00ae\u00af\b\4\1\2\u00af\u00b1\3"+
		"\2\2\2\u00b0\u00ab\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2"+
		"\u00b3\5\16\b\2\u00b3\u00b4\b\4\1\2\u00b4\u00b6\3\2\2\2\u00b5\u0089\3"+
		"\2\2\2\u00b5\u0091\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\7\6\2\2\u00b8"+
		"\u00ba\3\2\2\2\u00b9\u00b5\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00b9\3\2"+
		"\2\2\u00bb\u00bc\3\2\2\2\u00bc\7\3\2\2\2\u00bd\u00be\5\n\6\2\u00be\u00bf"+
		"\b\5\1\2\u00bf\u00c4\3\2\2\2\u00c0\u00c1\5\f\7\2\u00c1\u00c2\b\5\1\2\u00c2"+
		"\u00c4\3\2\2\2\u00c3\u00bd\3\2\2\2\u00c3\u00c0\3\2\2\2\u00c4\t\3\2\2\2"+
		"\u00c5\u00c6\7%\2\2\u00c6\u00cc\b\6\1\2\u00c7\u00c8\7&\2\2\u00c8\u00cc"+
		"\b\6\1\2\u00c9\u00ca\7(\2\2\u00ca\u00cc\b\6\1\2\u00cb\u00c5\3\2\2\2\u00cb"+
		"\u00c7\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cc\13\3\2\2\2\u00cd\u00ce\b\7\1"+
		"\2\u00ce\u00da\7\24\2\2\u00cf\u00d0\5\b\5\2\u00d0\u00d7\b\7\1\2\u00d1"+
		"\u00d2\7\4\2\2\u00d2\u00d3\5\b\5\2\u00d3\u00d4\b\7\1\2\u00d4\u00d6\3\2"+
		"\2\2\u00d5\u00d1\3\2\2\2\u00d6\u00d9\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d7"+
		"\u00d8\3\2\2\2\u00d8\u00db\3\2\2\2\u00d9\u00d7\3\2\2\2\u00da\u00cf\3\2"+
		"\2\2\u00da\u00db\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\7\25\2\2\u00dd"+
		"\u00de\7\'\2\2\u00de\u00df\5\n\6\2\u00df\u00e0\b\7\1\2\u00e0\r\3\2\2\2"+
		"\u00e1\u00e2\5\20\t\2\u00e2\u00f1\b\b\1\2\u00e3\u00e4\7\r\2\2\u00e4\u00e5"+
		"\5\20\t\2\u00e5\u00e6\b\b\1\2\u00e6\u00f0\3\2\2\2\u00e7\u00e8\7\16\2\2"+
		"\u00e8\u00e9\5\20\t\2\u00e9\u00ea\b\b\1\2\u00ea\u00f0\3\2\2\2\u00eb\u00ec"+
		"\7\f\2\2\u00ec\u00ed\5\20\t\2\u00ed\u00ee\b\b\1\2\u00ee\u00f0\3\2\2\2"+
		"\u00ef\u00e3\3\2\2\2\u00ef\u00e7\3\2\2\2\u00ef\u00eb\3\2\2\2\u00f0\u00f3"+
		"\3\2\2\2\u00f1\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\17\3\2\2\2\u00f3"+
		"\u00f1\3\2\2\2\u00f4\u00f5\5\22\n\2\u00f5\u0104\b\t\1\2\u00f6\u00f7\7"+
		"\17\2\2\u00f7\u00f8\5\22\n\2\u00f8\u00f9\b\t\1\2\u00f9\u0103\3\2\2\2\u00fa"+
		"\u00fb\7\20\2\2\u00fb\u00fc\5\22\n\2\u00fc\u00fd\b\t\1\2\u00fd\u0103\3"+
		"\2\2\2\u00fe\u00ff\7\n\2\2\u00ff\u0100\5\22\n\2\u0100\u0101\b\t\1\2\u0101"+
		"\u0103\3\2\2\2\u0102\u00f6\3\2\2\2\u0102\u00fa\3\2\2\2\u0102\u00fe\3\2"+
		"\2\2\u0103\u0106\3\2\2\2\u0104\u0102\3\2\2\2\u0104\u0105\3\2\2\2\u0105"+
		"\21\3\2\2\2\u0106\u0104\3\2\2\2\u0107\u0108\5\24\13\2\u0108\u0117\b\n"+
		"\1\2\u0109\u010a\7\7\2\2\u010a\u010b\5\24\13\2\u010b\u010c\b\n\1\2\u010c"+
		"\u0116\3\2\2\2\u010d\u010e\7\b\2\2\u010e\u010f\5\24\13\2\u010f\u0110\b"+
		"\n\1\2\u0110\u0116\3\2\2\2\u0111\u0112\7\t\2\2\u0112\u0113\5\24\13\2\u0113"+
		"\u0114\b\n\1\2\u0114\u0116\3\2\2\2\u0115\u0109\3\2\2\2\u0115\u010d\3\2"+
		"\2\2\u0115\u0111\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117"+
		"\u0118\3\2\2\2\u0118\23\3\2\2\2\u0119\u0117\3\2\2\2\u011a\u011b\7\21\2"+
		"\2\u011b\u017c\b\13\1\2\u011c\u011d\7\22\2\2\u011d\u017c\b\13\1\2\u011e"+
		"\u011f\7\23\2\2\u011f\u017c\b\13\1\2\u0120\u0121\7$\2\2\u0121\u017c\b"+
		"\13\1\2\u0122\u0123\7#\2\2\u0123\u0124\7(\2\2\u0124\u0125\7\24\2\2\u0125"+
		"\u0131\b\13\1\2\u0126\u0127\5\16\b\2\u0127\u012e\b\13\1\2\u0128\u0129"+
		"\7\4\2\2\u0129\u012a\5\16\b\2\u012a\u012b\b\13\1\2\u012b\u012d\3\2\2\2"+
		"\u012c\u0128\3\2\2\2\u012d\u0130\3\2\2\2\u012e\u012c\3\2\2\2\u012e\u012f"+
		"\3\2\2\2\u012f\u0132\3\2\2\2\u0130\u012e\3\2\2\2\u0131\u0126\3\2\2\2\u0131"+
		"\u0132\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0134\7\25\2\2\u0134\u017c\b"+
		"\13\1\2\u0135\u0136\7\31\2\2\u0136\u0137\5\16\b\2\u0137\u0138\7\32\2\2"+
		"\u0138\u0139\7\26\2\2\u0139\u013a\5\16\b\2\u013a\u013b\7\27\2\2\u013b"+
		"\u013c\7\33\2\2\u013c\u013d\7\26\2\2\u013d\u013e\5\16\b\2\u013e\u013f"+
		"\7\27\2\2\u013f\u0140\b\13\1\2\u0140\u017c\3\2\2\2\u0141\u0142\7\13\2"+
		"\2\u0142\u0143\7\24\2\2\u0143\u0144\5\16\b\2\u0144\u0145\7\25\2\2\u0145"+
		"\u0146\b\13\1\2\u0146\u017c\3\2\2\2\u0147\u0148\7\34\2\2\u0148\u0149\7"+
		"\24\2\2\u0149\u014a\5\16\b\2\u014a\u014b\7\25\2\2\u014b\u014c\b\13\1\2"+
		"\u014c\u017c\3\2\2\2\u014d\u014e\7\24\2\2\u014e\u014f\5\16\b\2\u014f\u0150"+
		"\7\25\2\2\u0150\u0151\b\13\1\2\u0151\u017c\3\2\2\2\u0152\u0153\7(\2\2"+
		"\u0153\u0179\b\13\1\2\u0154\u0155\7\24\2\2\u0155\u0161\b\13\1\2\u0156"+
		"\u0157\5\16\b\2\u0157\u015e\b\13\1\2\u0158\u0159\7\4\2\2\u0159\u015a\5"+
		"\16\b\2\u015a\u015b\b\13\1\2\u015b\u015d\3\2\2\2\u015c\u0158\3\2\2\2\u015d"+
		"\u0160\3\2\2\2\u015e\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0162\3\2"+
		"\2\2\u0160\u015e\3\2\2\2\u0161\u0156\3\2\2\2\u0161\u0162\3\2\2\2\u0162"+
		"\u0163\3\2\2\2\u0163\u0164\7\25\2\2\u0164\u017a\b\13\1\2\u0165\u0166\7"+
		"\30\2\2\u0166\u0167\7(\2\2\u0167\u0168\b\13\1\2\u0168\u0169\7\24\2\2\u0169"+
		"\u0175\b\13\1\2\u016a\u016b\5\16\b\2\u016b\u0172\b\13\1\2\u016c\u016d"+
		"\7\4\2\2\u016d\u016e\5\16\b\2\u016e\u016f\b\13\1\2\u016f\u0171\3\2\2\2"+
		"\u0170\u016c\3\2\2\2\u0171\u0174\3\2\2\2\u0172\u0170\3\2\2\2\u0172\u0173"+
		"\3\2\2\2\u0173\u0176\3\2\2\2\u0174\u0172\3\2\2\2\u0175\u016a\3\2\2\2\u0175"+
		"\u0176\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0178\7\25\2\2\u0178\u017a\b"+
		"\13\1\2\u0179\u0154\3\2\2\2\u0179\u0165\3\2\2\2\u0179\u017a\3\2\2\2\u017a"+
		"\u017c\3\2\2\2\u017b\u011a\3\2\2\2\u017b\u011c\3\2\2\2\u017b\u011e\3\2"+
		"\2\2\u017b\u0120\3\2\2\2\u017b\u0122\3\2\2\2\u017b\u0135\3\2\2\2\u017b"+
		"\u0141\3\2\2\2\u017b\u0147\3\2\2\2\u017b\u014d\3\2\2\2\u017b\u0152\3\2"+
		"\2\2\u017c\25\3\2\2\2%\35\"*\65EH^apu\177\u0086\u00a4\u00a7\u00b0\u00b5"+
		"\u00bb\u00c3\u00cb\u00d7\u00da\u00ef\u00f1\u0102\u0104\u0115\u0117\u012e"+
		"\u0131\u015e\u0161\u0172\u0175\u0179\u017b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
grammar FOOL;

@header{
import java.util.ArrayList;
import java.util.HashMap;
import ast.*;
import entry.*;
import interfaces.*;
import lib.*;
import types.*;
}

@parser::members{
private int nestingLevel = 0;
private int globalOffset=-2;

private ArrayList<HashMap<String,STentry>> symTable = new ArrayList<HashMap<String,STentry>>();

//Mappa il nome della classe alla sua virtual table
//Serve per preservare le dichiarazioni interne ad una classe una volta che il parser ha concluso la sua dichiarazione
private HashMap<String,HashMap<String,STentry>> classTable = new HashMap<String,HashMap<String,STentry>>();
}

@lexer::members {
int lexicalErrors=0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
prog returns [Node ast]
: {	HashMap<String,STentry> hm = new HashMap<String,STentry> ();
       symTable.add(hm);
       boolean isClassList = false;
       boolean isDecList = false;
       } 
       ( LET ( c=cllist {isClassList=true;} (d=declist {isDecList=true;})?  										
        	  | d=declist {isDecList = true;} 
              ) IN ex=exp
        | e=exp
            {$ast = new ProgNode($e.ast);} 
        ) 
        {
  	      	if(isClassList == true && isDecList == true){
    			$ast = new ProgLetInNode($c.astlist, $d.astlist, $ex.ast); 
    		}else if(isClassList == true && isDecList == false){
    			$ast = new ProgLetInNode($c.astlist, new ArrayList<DecNode>(), $ex.ast);
        	}else if(isClassList == false && isDecList == true){
        		$ast = new ProgLetInNode(new ArrayList<DecNode>(), $d.astlist, $ex.ast);
        	}else{
        		$ast = new ProgLetInNode(new ArrayList<DecNode>(), new ArrayList<DecNode>(), $ex.ast);
        	}  
        	symTable.remove(nestingLevel);
        }
        SEMIC
      ;


//per le classi il nesting level � 0, cio� l'ambiente globale		      
cllist returns [ArrayList<DecNode> astlist]
	:	{$astlist= new ArrayList<DecNode>(); }//lista delle classi 		 
		 //Dichiarazione di classe
		(CLASS i=ID{
			
				HashMap<String,STentry> hm = symTable.get(0); //nome della classe mappato a una nuova STentry(recuperato a livello 0 symbol table)
				
				STentry classEntry = new STentry(nestingLevel, globalOffset--); //decremento offset ogni volta che si incontra una nuova dichiarazione di classe
				
				 if (hm.put($i.text, classEntry) != null){ 
		         	System.out.println("Class id "+$i.text+" at line "+$i.line+" already declared");
		          	System.exit(0);
		      	}
 		      	
 		      	HashMap<String,STentry> virtualTable = new HashMap<String,STentry>(); //conterr� le informazioni della classe
 		      	
 		      	int offsetFields = -1; //parto da -1 e decremento
 		      	int offsetMethods = 0; //parto da 0 ed incremento

				//la classe non eredita da altre classi
				//il tipo � un nuovo oggetto ClassTypeNode con una lista inizialmente vuota in allFields e allMethods	
	
				ArrayList<Node> allFields = new ArrayList<Node>();
				ArrayList<Node> allMethods = new ArrayList<Node>();
				
				//creo nodo di tipo classe con il nome della classe
				ClassTypeNode classType = new ClassTypeNode($i.text, allFields, allMethods);	
				
				//creo nodo classe
				ClassNode c = new ClassNode(classType);
				
				//aggiungo la classe alla lista delle classi
				$astlist.add(c); 
		 		      	
		  	}
		  	
			(EXTENDS ID)? //non implementata (versione senza ereditariet�)
			
			LPAR{
				
				//aggiungo il tipo ClassTypeNode alla entry della classe
				classEntry.addType(classType);
				
				//setto alla classe il tipo messo in symbol table
			 	c.setSymType(classType);
			 	
			 	//nella Class Table viene aggiunto il nome della classe
				//mappato ad una nuova Virtual Table creata vuota perch� non si eredita
				classTable.put($i.text, virtualTable);
				
				//aumento il livello perch� il parser � dentro una classe
				nestingLevel++; //dentro le classi il nesting level � sempre 1
			
			 	//la SymbolTable per il livello corrispondente (livello 1) deve includere anche la VirtualTable
				symTable.add(virtualTable);  
			 }
			 			
		    // ora scorro gli elementi della classe e aggiorno la virtualTable ad ogni campo/metodo incontrato
		    // Costruttore della classe: contiene i campi della classe che devono essere inizializzati
		    // alla creazione degli oggetti in quanto immutabili
			 (pfid=ID COLON pft=type {
			 	
			 	//primo campo del costruttore,quindi creo un nuovo campo come nodo dell'AST
				FieldNode field = new FieldNode($pfid.text, $pft.ast);	
				
				//recupero il contenuto di allFields
				allFields = classType.getAllFields();	
				
				//creo una nuova entry per il nodo del campo
				STentry newFieldEntry;
				
				newFieldEntry = new STentry(nestingLevel,  $pft.ast , offsetFields--);  //si parte da -1 come da layout e si decrementa
				allFields.add(field); //aggiungo il campo alla lista di tutti i campi
				classType.setAllFields(allFields);

				//aggiungo alla virtual table la entry del campo mappata al nome del campo
				virtualTable.put($pfid.text, newFieldEntry); 

							
			 } (COMMA pnid=ID COLON pnt=type{ 
			 	
			 		//tutti gli altri campi del costruttore	
			 		FieldNode fieldN = new FieldNode($pnid.text, $pnt.ast);	
			 		
					allFields = classType.getAllFields();	

					newFieldEntry = new STentry(nestingLevel, $pnt.ast, offsetFields--);
					allFields.add(fieldN);
					classType.setAllFields(allFields);

					virtualTable.put($pnid.text, newFieldEntry);
			
			 	})* //ripeto per tutti i campi	
			 	
			 )? RPAR 
			 //dichiarazione dei metodi   
		      CLPAR
		         (FUN mid=ID COLON mt=type {
		         	
		         	MethodNode method = new MethodNode($mid.text, $mt.ast); 
		         	
		         	//memorizzo il tipo dei parametri il quale andr� insieme al tipo di ritorno
                    //a comporre il tipo complessivo del metodo
                    
                    ArrayList<Node> parTypes = new ArrayList<Node>();
					ArrayList<Node> varTypes = new ArrayList<Node>(); //lista di variabili (dichiarazioni non funzionali)
					
					//incremento il nestingLevel dentro lo scope del metodo
					nestingLevel++;
					
					//creare una nuova hashmap per il metodo e la aggiungo alla symbol table
					HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
					symTable.add(hmn);
					
					int parOffset = 1; //offset dei parametri(iniziano da 1 ed incrementano)
					int varOffset = -2; //dichiarazioni da -2 e decremento (ad offset 0 si trova l'access link mentre al -1 il RA)
					
					STentry newMethodEntry;
					
					//recupero l'array di allMethods
					allMethods = classType.getAllMethods();	
      		         
      		        //inserisco nuovo metodo        
					newMethodEntry = new STentry(nestingLevel, $mt.ast, offsetMethods++); //offset dei metodi incrementato ogni nuova dichiarazione di metodo
					newMethodEntry.setMethod();//setto come metodo per distinguerlo dai campi
					method.setOffset(offsetMethods); //durante il parsing viene settato il campo offset al metodo
					allMethods.add(method); //aggiungo il nuovo metodo alla lista dei metodi
					classType.setAllMethods(allMethods);//lo metto in classtypenode
											

	         	}LPAR
	         	 (mpfid=ID COLON mpfty=hotype{ //tipi funzionali
		         		
						ParNode firstPar = new ParNode($mpfid.text,$mpfty.ast); 
						method.addPar(firstPar); //aggiungo il parametro al metodo
						parTypes.add($mpfty.ast); //aggiungo il parametro alla lista dei parametri
				         		
						//nel caso in cui ci siano parametri funzionali devo riservare due spazi
						if($mpfty.ast instanceof ArrowTypeNode){
							parOffset++;
						}
						
						//verifico se il primo parametro � gi� presente nella symbol table
						if (hmn.put($mpfid.text, new STentry(nestingLevel, $mpfty.ast, parOffset++)) != null){
							System.out.println("Parameter id "+$mpfid.text+" at line "+$mpfid.line+" already declared");
							System.exit(0);
						}
						
	         		}(COMMA mpnid=ID COLON mpnty=hotype{ //parametri successivi
	  
		         			if($mpnty.ast instanceof ArrowTypeNode){ //se il parametro � funzionale, deve occupare due spazi  
								parOffset++;                 
					        }		    	
						    ParNode nextPar = new ParNode($mpnid.text, $mpnty.ast);
						    method.addPar(nextPar); //aggiungo il paramentro al metodo
						    parTypes.add($mpnty.ast);	

						    if (hmn.put($mpnid.text,new STentry(nestingLevel, $mpnty.ast, parOffset++)) != null){
						    	System.out.println("Parameter id "+$mpnid.text+" at line "+$mpnid.line+" already declared");
						     	System.exit(0);
					     	}
	     		  		})*
		         	)? RPAR
		         	//scope del metodo con dichiarazioni di variabili
		                (LET	         			 
     						(VAR vid=ID COLON vt=type ASS ve=exp SEMIC{ //le variabili possono esserci o non esserci
					          
	     					 	VarNode var = new VarNode($vid.text, $vt.ast, $ve.ast);  
	     					 	varTypes.add(var); 
                             
								if (hmn.put($vid.text, new STentry(nestingLevel, $vt.ast, varOffset--)) != null){
							     	System.out.println("Var id "+$vid.text+" at line "+$vid.line+" already declared");
							      	System.exit(0);
							  	}  
							  	
     					 	})*
     					 	{ 
 					 		//aggiungo lista di dichiarazioni al metodo
 					 		method.addDec(varTypes);
 					 		
				 		} IN)? 
 					 	{	
 					 		ArrowTypeNode methodType = new ArrowTypeNode(parTypes,$mt.ast); //nodo che rappresenta il tipo funzionale del metodo
          					newMethodEntry.addType(methodType); //aggiungo il tipo al metodo
							virtualTable.put($mid.text, newMethodEntry);
 					 	}
 					 	me=exp {method.addBody($me.ast);} 
			       SEMIC {
			       	symTable.remove(nestingLevel--);//fine scope metodo, rimuovo il livello corrente symbol table
			       } 
			     )*   //possono esserci pi� metodi             
		      CRPAR
		      { //All�uscita dalla dichiarazione della classe rimuovo livello corrente symbol table
		      	 symTable.remove(nestingLevel--);
		      }
		  )+ //potrebbero esserci altre classi
		; 
		         	

declist	returns [ArrayList<DecNode> astlist]
	: {$astlist = new ArrayList<DecNode>();
	    int offset=-2;
	    if(nestingLevel==0){
	    	offset = globalOffset;
	    }
      }		
	  ( 
	  	( VAR i=ID COLON ho=hotype ASS e=exp {
	  		 VarNode v = new VarNode($i.text,$ho.ast,$e.ast);  
             $astlist.add(v);                                 
             HashMap<String,STentry> hm = symTable.get(nestingLevel);
             if (hm.put($i.text, new STentry(nestingLevel,$ho.ast,offset--)) != null){
	             	System.out.println("Var id "+$i.text+" at line "+$i.line+" already declared");
	              	System.exit(0);
              	}
              	
           		//con higher order offset decrementato 2 volte (per i tipi funzionali)
	           if($ho.ast instanceof ArrowTypeNode){
	           	   offset--;
	           }  
    		}   
        | FUN i=ID COLON t=type{ //inserimento di ID nella symbol table
	           FunNode f = new FunNode($i.text,$t.ast); //nuovo nodo funzione
	           $astlist.add(f);                              
	           HashMap<String,STentry> hm = symTable.get(nestingLevel);
	           STentry entry = new STentry(nestingLevel,offset--);  
	           offset--;
	           if (hm.put($i.text,entry) != null){
	               	System.out.println("Fun id "+$i.text+" at line "+$i.line+" already declared");
	                System.exit(0);
	            }
	            //creare una nuova hashmap per la symTable
	            nestingLevel++; //scope della funzione
	            HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
	            symTable.add(hmn);
            }       
            LPAR {
            	 ArrayList<Node> parTypes = new ArrayList<Node>();
          	     int paroffset=1;
            }
            (fid=ID COLON fty=hotype{
            	parTypes.add($fty.ast);
                ParNode fpar = new ParNode($fid.text,$fty.ast); //creo nodo ParNode
                f.addPar(fpar);                                 //lo attacco al FunNode con addPar
				//nel caso in cui ci siano parametri funzionali devo riservare due spazi
				if($fty.ast instanceof ArrowTypeNode){
					paroffset++;
				}
				if (hmn.put($fid.text,new STentry(nestingLevel,$fty.ast,paroffset++)) != null  ){ //aggiungo dichiarazione a hmn
					System.out.println("Parameter id "+$fid.text+" at line "+$fid.line+" already declared");
					System.exit(0);
				}
			}
			(COMMA id=ID COLON ty=hotype{
				if($ty.ast instanceof ArrowTypeNode){ //se il parametro � funzionale, deve occupare due spazi  
					paroffset++;                 
		        }				    	
			    parTypes.add($ty.ast);
			    ParNode par = new ParNode($id.text,$ty.ast);
			    f.addPar(par);
			    if ( hmn.put($id.text,new STentry(nestingLevel,$ty.ast,paroffset++)) != null  ){
			    	System.out.println("Parameter id "+$id.text+" at line "+$id.line+" already declared");
			     	System.exit(0);
		     	}
		    })*)? 
              RPAR {
              	entry.addType(new ArrowTypeNode(parTypes,$t.ast));
              	// aggiungo il tipo anche al FunNode
              	f.addSymType(new ArrowTypeNode(parTypes,$t.ast));
              }
              (LET d=declist IN {f.addDec($d.astlist);})? e=exp{
              	f.addBody($e.ast);
               //rimuovere la hashmap corrente poich� esco dallo scope               
               symTable.remove(nestingLevel--);    
              }
      ) SEMIC	
    )+      
	;
	
	
hotype returns [Node ast] //tipi higher order
	: t=type {$ast=$t.ast;}
	| a=arrow {$ast=$a.ast;}
	;
	
type returns [Node ast]
	: INT  {$ast=new IntTypeNode();}
	| BOOL {$ast=new BoolTypeNode();}
	| id=ID {$ast=new RefTypeNode($id.text);} //tipo della classe ID
	;	
	
arrow returns [Node ast]
	: {ArrayList<Node> parList = new ArrayList<Node>();} //lista dei parametri
	   LPAR (ho=hotype {parList.add($ho.ast);} (COMMA ho=hotype {parList.add($ho.ast);})* )? RPAR ARROW t=type
  		// il tipo della funzione da ritornare
  	  {$ast = new ArrowTypeNode(parList,$t.ast);}
  	;

exp	returns [Node ast]
 	: f=term {$ast= $f.ast;}
 	    (PLUS l=term
 	     {$ast= new PlusNode ($ast,$l.ast);}
 	     | MINUS l2=term
 	     {$ast= new MinusNode ($ast,$l2.ast);}
 	     | OR l3=term
 	     {$ast= new OrNode ($ast,$l3.ast);}
 	    )*
 	; 
 	
term returns [Node ast]
	: f=factor {$ast= $f.ast;}
	    (TIMES l=factor
	     {$ast= new MultNode ($ast,$l.ast);}
	     | DIV l2=factor
	     {$ast= new DivNode ($ast,$l2.ast);}
	     | AND l3=factor
	     {$ast= new AndNode ($ast,$l3.ast);}
	    )*
	;
	
factor returns [Node ast]
	: f=value {$ast= $f.ast;}
	    (EQ l=value 
	     {$ast= new EqualNode($ast,$l.ast);}
	     | LE l2=value
	     {$ast= new LesserEqualNode($ast,$l2.ast);}
	     | GE l3=value
	     {$ast= new GreaterEqualNode($ast,$l3.ast);}
	    )*
 	;	     
    	
value returns [Node ast]
	: n=INTEGER   
	  {$ast= new IntNode(Integer.parseInt($n.text));}  
	| TRUE	{$ast = new BoolNode(true);}  
	| FALSE	{$ast = new BoolNode(false);}  
    | NULL	{$ast = new EmptyNode();}
    | NEW nid=ID LPAR{
	    	ArrayList<Node> parList = new ArrayList<Node>(); //creo lista parametri
	    	STentry entry = (symTable.get(0)).get($nid.text); //STentry presa direttamente da livello 0 della Symbol Table
	    	if(classTable.get($nid.text) == null || entry == null){ //se non � in classTable o � nullo
	    		System.out.println("Error: class "+$nid.text +" at line "+ $nid.line +" not declared!");
	          	System.exit(0);
			}
    	}(fne=exp{parList.add($fne.ast);} //aggiungo il primo parametro poi tutti gli n parametri se ci sono
    		(COMMA nne=exp{parList.add($nne.ast);})*
    	)? RPAR {$ast = new NewNode($nid.text,entry,parList);} //resituisco newNode    	
	| IF x=exp THEN CLPAR y=exp CRPAR ELSE CLPAR z=exp CRPAR {$ast= new IfNode($x.ast,$y.ast,$z.ast);}
	| NOT LPAR e=exp RPAR {$ast= new NotNode($e.ast);}	 
	| PRINT LPAR e=exp RPAR	{$ast= new PrintNode($e.ast);}
	| LPAR e=exp RPAR {$ast= $e.ast;}  
	| i=ID {
		//cercare la dichiarazione
       int j = nestingLevel;
       STentry entry = null; 
       while (j>=0 && entry==null){
       	entry = (symTable.get(j--)).get($i.text); //recupero la entry decrementando il nestring level
       }
       if(entry == null){
       	System.out.println("Error: id "+$i.text+" at line "+$i.line+" not declared");
       	System.exit(0);
       }          
    	$ast= new IdNode($i.text,entry,nestingLevel);} 
	   //Chiamata a funzione
	   (LPAR {ArrayList<Node> pList = new ArrayList<Node>();} 
	   	 (a=exp {pList.add($a.ast);} 
	   	 	(COMMA a=exp {pList.add($a.ast);} )*
	   	 )? 
	   	 RPAR {$ast= new CallNode($i.text,entry,pList,nestingLevel);}
	   	 //Invoco il metodo
	    | DOT mi=ID{
			//controllo che la entry sia i tipo RefTypeNode
	    	if(!(entry.getType() instanceof RefTypeNode)){
		       	System.out.println("Error: id "+$mi.text+" at line "+$mi.line+" is not an object");
       			System.exit(0);
	    	}	
	    	
	    	String classId = ((RefTypeNode) entry.getType()).getIdClass(); //tipo della classe
	    	STentry methodEntry = classTable.get(classId).get($mi.text); //recupero il metodo

	    	if(methodEntry == null){
		       	System.out.println("Error: Method "+$mi.text+" at line "+$mi.line+" not declared");
       			System.exit(0);	    		
	    	}
	    	
	  	  } LPAR {ArrayList<Node> parList = new ArrayList<Node>();} //parametri del metodo
				(mfe=exp {parList.add($mfe.ast);} (COMMA mne=exp {parList.add($mne.ast);})* )? RPAR 
			//creo il nodo da restituire
			{$ast = new ClassCallNode($i.text, nestingLevel, entry, methodEntry, classId, parList);}
	   )?
 	; 
  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

COLON	: ':' ;
COMMA	: ',' ;
ASS	    : '=' ;
SEMIC   : ';' ;
EQ      : '==' ;
LE      : '<=' ;
GE      : '>=' ;
AND     : '&&' ;
NOT     : '!' ;
OR      : '||';
PLUS	: '+' ;
MINUS   : '-' ;
TIMES	: '*' ;
DIV     : '/';
INTEGER : ('-')?(('1'..'9')('0'..'9')*) | '0';
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
CLPAR 	: '{' ;
CRPAR	: '}' ;
DOT     : '.' ;
IF 	    : 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET	    : 'let' ;
IN	    : 'in' ;
VAR	    : 'var' ;
FUN	    : 'fun' ;
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ;
 
ID 	: ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ; //
 
WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR     : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
